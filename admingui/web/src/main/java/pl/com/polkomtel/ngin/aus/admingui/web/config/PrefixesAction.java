package pl.com.polkomtel.ngin.aus.admingui.web.config;

import org.apache.commons.logging.Log;

import pl.com.polkomtel.ngin.aus.admingui.core.config.util.ValidationError;
import org.apache.struts2.interceptor.validation.SkipValidation;
import pl.com.polkomtel.ngin.aus.admingui.core.config.model.Prefix;
import org.apache.commons.logging.LogFactory;

import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingException;

public class PrefixesAction extends AusBaseAdminGuiAction<Prefix> {

    private static final Log LOGGER = LogFactory.getLog(PrefixesAction.class);

    private static final long serialVersionUID = 1L;

    /**
     * Main
     *
     * @return status as String
     */
    @SkipValidation
    public String main() {
        return SUCCESS;
    }


    @Override
    public String saveNewEntity() {
        try {
            aus.validatePrefix(getEntity());
            aus.addPrefix(getEntity());
        } catch(NameAlreadyBoundException e) {
            addActionErrorByKey("prefixes.error.exists");
            LOGGER.error("Error, trying to save duplicate entry ", e);
            return INPUT;
        } catch (NamingException e) {
            addActionError(e.getMessage());
            LOGGER.error("Error when trying to save new prefix ", e);
            return ERROR;
        } catch (ValidationError e) {
            LOGGER.error("Validation failed when saving new entry ", e);
            addActionError(e.getMessage());
            return INPUT;
        }
        return SUCCESS;
    }

    public boolean getIsPrefixesAction() {
        return !isCurrentAction("main");
    }

    @Override
    public String editEntity() {
        return entityDetails();
    }

    @Override
    public String listEntities() {
        try {
            setEntityList(aus.listPrefixes(getQueryPattern()));
        } catch (NamingException e) {
            addActionError(e.getMessage());
            LOGGER.error("Error when trying to list prefixes ", e);
            return ERROR;
        }
        return SUCCESS;
    }

    @Override
    public String saveEditedEntity() {
        LOGGER.debug("saveEdited()");
        getEntity().setPrefixNumber(getId());
        try {
            aus.validatePrefix(getEntity());
            aus.editPrefix(getId(), getEntity());
        } catch (ValidationError e) {
            LOGGER.error("Validation failed when saving edited entry ", e);
            addActionError(e.getMessage());
            return INPUT;
        } catch (NamingException e) {
            addActionError(e.getMessage());
            LOGGER.error("Error while saving edited entity ", e);
            return ERROR;
        }
        return SUCCESS;
    }

    @Override
    public String entityDetails() {
        LOGGER.debug("entityDetails()");
        try {
            Prefix px = aus.detailsPrefix(this.getId());
            setEntity(px);
        } catch (NamingException e) {
            addActionError(e.getMessage());
            LOGGER.error("Error while retrieving details ", e);
            return ERROR;
        }
        return SUCCESS;
    }

    @Override
    public String deleteEntity() {
        LOGGER.debug("deleteEntity()");
        try {
            aus.canDeletePrefix(this.getId());
            aus.deletePrefix(this.getId());
        } catch (ValidationError e) {
            LOGGER.error("Validation failed when deleting prefix ", e);
            addActionError(e.getMessage());
            return INPUT;
        } catch (NamingException e) {
            addActionError(e.getMessage());
            LOGGER.error("Error while deleting ", e);
            return ERROR;
        }
        return SUCCESS;
    }

    @Override
    public Prefix getDefaultEntity() {
        return new Prefix();
    }
}
