package pl.com.polkomtel.ngin.aus.admingui.web.config;

import pl.com.polkomtel.ngin.aus.admingui.core.config.model.*;
import pl.com.polkomtel.ngin.aus.admingui.core.config.util.ValidationError;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingException;
import java.util.List;

public class ProfilesAction extends AusBaseAdminGuiAction<AusProfile> {

    private static final long serialVersionUID = 1L;
    private static final Log LOGGER = LogFactory.getLog(ProfilesAction.class);

    private List<Prefix> prefixList;
    private List<AnnWrapper> announcementList;
    private List<SciWrapper> sciList;

    private List<FciWrapper> fciList;

    @Override
    public String saveNewEntity() {
        prepareSuggestions();
        try {
            aus.validateAusProfile(getEntity());
            aus.addAusProfile(getEntity());
        } catch(NameAlreadyBoundException e) {
            addActionErrorByKey("profiles.error.exists");
            LOGGER.error("Tried to use existing ID", e);
            return INPUT;
        } catch (NamingException e) {
            addActionError(e.getMessage());
            LOGGER.error("Error during saving new record ", e);
            return ERROR;
        } catch (ValidationError e) {
            addActionError(e.getMessage());
            LOGGER.error("Validation failed when creating new profile", e);
            return INPUT;
        }
        return SUCCESS;
    }

    @Override
    public String newEntity() {
        this.entity = getDefaultEntity();
        prepareSuggestions();
        return SUCCESS;
    }

    private void prepareSuggestions() {
        try {
            this.prefixList = aus.listPrefixes("*");
            this.sciList = aus.getAllSci();
            this.fciList = aus.getAllFci();
            this.announcementList = aus.getAllAnnouncements();
        } catch (NamingException e) {
            // do nothing, it's just suggestions
            LOGGER.error("Prefix list couldn't load for suggestions", e);
        }
    }

    public boolean getIsProfilesAction() {
        return true;
    }

    @Override
    public String editEntity() {
        prepareSuggestions();
        return entityDetails();
    }

    @Override
    public String listEntities() {
        try {
            setEntityList(aus.listAusProfiles(getQueryPattern()));
        } catch (NamingException e) {
            addActionError(e.getMessage());
            LOGGER.error("Error while trying to list records ", e);
            return ERROR;
        }
        return SUCCESS;
    }

    @Override
    public String saveEditedEntity() {
        LOGGER.debug("saveEdited()");
        prepareSuggestions();
        getEntity().setMsisdn(getId());
        try {
            aus.validateAusProfile(getEntity());
            aus.editAusProfile(getId(), getEntity());
        } catch (NamingException e) {
            addActionError(e.getMessage());
            LOGGER.error("Error during saving edited record", e);
            return ERROR;
        } catch (ValidationError e) {
            LOGGER.error("Validation failed when editing profile", e);
            addActionError(e.getMessage());
            return INPUT;
        }
        return SUCCESS;
    }

    @Override
    public String entityDetails() {
        LOGGER.debug("entityDetails()");
        try {
            AusProfile ap = aus.detailsAusProfile(this.getId());
            setEntity(ap);
        } catch (NamingException e) {
            addActionError(e.getMessage());
            LOGGER.error("Error while trying to get record details ", e);
            return ERROR;
        }
        return SUCCESS;
    }

    @Override
    public String deleteEntity() {
        LOGGER.debug("deleteEntity()");
        try {
            aus.deleteAusProfile(this.getId());
        } catch (NamingException e) {
            addActionError(e.getMessage());
            LOGGER.error("Error while trying to delete record ", e);
            return ERROR;
        }
        return SUCCESS;
    }

    @Override
    public AusProfile getDefaultEntity() {
        AusProfile ausP = new AusProfile();
        ausP.setChargingFciServIdent(aus.getDefaultFciServIdent());
        return ausP;
    }

    public void setPrefixList(List<Prefix> prefixList) {
        this.prefixList = prefixList;
    }

    public void setAnnouncementList(List<AnnWrapper> announcementList) {
        this.announcementList = announcementList;
    }

    public void setSciList(List<SciWrapper> sciList) {
        this.sciList = sciList;
    }

    public List<Prefix> getPrefixList() {
        return prefixList;
    }

    public List<AnnWrapper> getAnnouncementList() {
        return announcementList;
    }

    public List<SciWrapper> getSciList() {
        return sciList;
    }

    public List<FciWrapper> getFciList() {
        return fciList;
    }

    public void setFciList(List<FciWrapper> fciList) {
        this.fciList = fciList;
    }
}
