package pl.com.polkomtel.ngin.aus.admingui.web.config;

import com.googlecode.scopeplugin.ScopeType;
import com.googlecode.scopeplugin.annotations.In;
import com.googlecode.scopeplugin.annotations.Out;
import com.nsn.ploc.feniks.admingui.struts.action.BaseAdminGuiListAction;
import com.nsn.ploc.feniks.admingui.struts.annotation.InjectEJB;
import pl.com.polkomtel.ngin.aus.admingui.core.config.service.AusManager;

import java.util.List;

public abstract class AusBaseAdminGuiAction<T> extends BaseAdminGuiListAction {

    private static final long serialVersionUID = 1L;
    private static final int DEFAULT_MAX_SEARCH_RESULTS = 50;
    protected T entity;
    private List<T> entityList;
    private String id;
    private boolean incompleteSearch;
    private int maxSearchResults = DEFAULT_MAX_SEARCH_RESULTS;
    private int totalMatching;

    @InjectEJB(AusManager.LOCAL_JNDI_NAME)
    protected transient AusManager aus;

    @In(scope = ScopeType.SESSION)
    @Out(scope = ScopeType.SESSION)
    private String queryPattern;

    public AusBaseAdminGuiAction() {
        super();
    }

    public String getQueryPattern() {
        return queryPattern;
    }

    public void setQueryPattern(String queryPattern) {
        this.queryPattern = queryPattern;
    }

    public int getMaxSearchResults() {
        return maxSearchResults;
    }

    public void setMaxSearchResults(int maxSearchResults) {
        this.maxSearchResults = maxSearchResults;
    }

    public int getTotalMatching() {
        return totalMatching;
    }

    public void setTotalMatching(int totalMatching) {
        this.totalMatching = totalMatching;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<T> getEntityList() {
        return entityList;
    }

    public void setEntityList(List<T> entityList) {
        this.totalMatching = entityList.size();
        if (totalMatching > maxSearchResults) {
            this.entityList = entityList.subList(0, maxSearchResults);
            setIncompleteSearch(true);
        } else {
            this.entityList = entityList;
            setIncompleteSearch(false);
        }
    }

    /**
     * Check is user admin
     *
     * @return is user admin
     */
    public boolean getIsAdmin() {
        return getRequest().isUserInRole("ADMIN") || getRequest().isUserInRole("AUSADMIN");
    }

    public abstract T getDefaultEntity();

    public T getEntity() {
        if (this.entity == null) {
            this.entity = getDefaultEntity();
        }
        return this.entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    public String listEntities() {
        return SUCCESS;
    }

    public String newEntity() {
        this.entity = getDefaultEntity();
        return SUCCESS;
    }

    public String saveNewEntity() {
        return SUCCESS;
    }

    public String entityDetails() {
        return SUCCESS;
    }

    public String editEntity() {
        return SUCCESS;
    }

    public String saveEditedEntity() {
        return SUCCESS;
    }

    public String deleteEntity() {
        return SUCCESS;
    }

    public boolean isIncompleteSearch() {
        return incompleteSearch;
    }

    public void setIncompleteSearch(boolean incompleteSearch) {
        this.incompleteSearch = incompleteSearch;
    }
}
