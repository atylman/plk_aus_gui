<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/content/taglibs.jsp" %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="Expires" content="0"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Cache-Control" content="no-store"/>
    <meta name="Robots" content="noindex, nofollow"/>

   	<s:url var="resourcesAppUrl" value="/nsn-admingui-resources" includeContext="false"/>

    <script type="text/javascript" src="${resourcesAppUrl}/js/jquery-1.5.1.min.js"></script>
	<script type="text/javascript" src="${resourcesAppUrl}/js/log.js"></script>
	<script type="text/javascript" src="${resourcesAppUrl}/js/autoheight.js"></script>
	<script type="text/javascript" src="${resourcesAppUrl}/js/autocompleter/listSetsFunctions.js"></script>
	<script type="text/javascript" src="${resourcesAppUrl}/js/jquery.numeric.js"></script>

	<link type="text/css" rel="stylesheet" href="${resourcesAppUrl}/css/admingui.css"/>

	<link type="text/css" rel="stylesheet" href="${resourcesAppUrl}/css/shared.css"/>
	<link type="text/css" rel="stylesheet" href="${resourcesAppUrl}/css/tabs.css"/>

	<script type="text/javascript" src="${pageContext.request.contextPath}/js/aus.js"></script>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/aus.css"/>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/chosen.css"/>

    <decorator:head/>
</head>
<body>
	<div class="main">
		<div class="module-tabs">
			<s:if test="isPrefixesAction">
				<a class="tab current" href="<s:url action='listPrefixes'/>" ><s:text name="prefixes.menulabel"/></a>
			</s:if>
			<s:else>
				<a class="tab" href="<s:url action='listPrefixes'/>" ><s:text name="prefixes.menulabel"/></a>
			</s:else>


			<s:if test="isProfilesAction">
				<a class="tab current" href="<s:url action='listProfiles'/>" ><s:text name="profiles.menulabel"/></a>
			</s:if>
			<s:else>
				<a class="tab" href="<s:url action='listProfiles'/>" ><s:text name="profiles.menulabel"/></a>
			</s:else>
		</div>

		<s:actionerror escape="false"/>
		<s:fielderror escape="false"/>

       	<decorator:body/>
	</div>
	<script type="text/javascript">
		if(parent) {
			parent.autofitIframe();
		}
	</script>
	<script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/js/chosen.jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript">
	  $('.chosen-select').chosen();
	</script>
</body>
</html>