<%@ include file="/WEB-INF/content/taglibs.jsp"%>

<table class="elements-list" style="width: 670px">
	<thead>
		<tr>
			<th colspan="2" class="table-title">
			<s:text name="prefixes.details" /></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 200px"><s:text name="prefixes.id" /></td>
			<td><s:property value="entity.prefixNumber" /></td>
		</tr>
		<tr>
			<td><s:text name="prefixes.description" /></td>
			<td><s:property value="entity.description" /></td>
		</tr>
	</tbody>
</table>
<div class="formControls">
	<common:ifUserInRole roles="ADMIN">
		<s:a action="editPrefix">
			<s:text name="operation.modify" />
			<s:param name="id" value="entity.prefixNumber" />
		</s:a>
    &nbsp;|&nbsp;
    <s:a action="deletePrefix" cssClass="delete">
			<s:text name="operation.delete" />
			<s:param name="id" value="entity.prefixNumber" />
		</s:a>
    &nbsp;|&nbsp;
  </common:ifUserInRole>
	<s:a action="listPrefixes">
		<s:text name="operation.back" />
	</s:a>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		$('.delete').bind('click', function(event) {
			event.preventDefault();
			if (confirm('<s:text name="delete.confirm.message"/>')) {
				document.location = $(this).attr('href');
			}
		});
	});
</script>