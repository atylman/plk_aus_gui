<%@ include file="/WEB-INF/content/taglibs.jsp"%>

<br />

<s:form>
	<table class="elements-list" style="width: 670px">
		<thead>
			<tr>
				<th colspan="2" class="table-title"><s:text name="prefixes.new" /></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="width: 200px"><s:text name="prefixes.id" /></td>
				<td><s:textfield key="entity.prefixNumber" /></td>
			</tr>
			<tr>
				<td><s:text name="prefixes.description" /></td>
				<td><s:textarea key="entity.description" cols="40" rows="4"/></td>
			</tr>
		</tbody>
	</table>
	<s:submit value="%{getText('operation.save')}"
		action="saveNewPrefix" />
	<s:submit value="%{getText('operation.cancel')}" action="listPrefixes" />
</s:form>