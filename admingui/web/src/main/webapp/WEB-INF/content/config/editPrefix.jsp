<%@ include file="/WEB-INF/content/taglibs.jsp"%>

<br />

<s:form>
	<table class="elements-list" style="width: 670px">
		<thead>
			<tr>
				<th colspan="2" class="table-title"><s:text name="prefixes.edit" /></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="width: 200px"><s:text name="prefixes.id" />
				</td>
				<td><s:hidden name="id" value="%{entity.prefixNumber}" /> <s:property
						value="entity.prefixNumber" /></td>
			</tr>
			<tr>
				<td><s:text name="prefixes.description" /></td>
				<td><s:textarea key="entity.description" cols="40" rows="4"/></td>
			</tr>
		</tbody>
	</table>
	<s:submit value="%{getText('operation.update')}" action="saveEditedPrefix" />
	<s:submit value="%{getText('operation.cancel')}" action="listPrefixes" />
</s:form>