<%@ include file="/WEB-INF/content/taglibs.jsp"%>

<br />

<s:form>
	<table class="elements-list" style="width: 670px">
		<thead>
			<tr>
				<th colspan="2" class="table-title"><s:text name="profiles.new" /></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="width: 200px"><s:text name="profiles.id" /></td>
				<td><s:textfield key="entity.msisdn" /></td>
			</tr>
			<tr>
				<td><s:text name="profiles.isblocked" /></td>
				<td><s:checkbox key="entity.isBlocked"/></td>
			</tr>
            <tr>
				<td><s:text name="prefixes.id" /></td>
				<td>
					<s:select label="prefixes.id" list="prefixList" listKey="prefixNumber" listValue="optionString"
						value="entity.prefixNumber" data-placeholder="Select a prefix..." cssClass="chosen-select"
						emptyOption="true" cssStyle="width:350px;" name="entity.prefixNumber"/>
				</td>
			</tr>
            <tr>
				<td><s:text name="profiles.annid" /></td>
				<td>
					<s:select label="profiles.annid" list="announcementList" listKey="announcement.id" listValue="optionString"
					value="entity.announcementId" data-placeholder="Select announcement..." cssClass="chosen-select"
                    emptyOption="true" cssStyle="width:350px;" name="entity.announcementId"/>
				</td>
			</tr>
            <tr>
				<td><s:text name="profiles.fciid" /></td>
				<td>
					<s:select label="profiles.fciid" list="fciList" listKey="fci.fciId" listValue="optionString"
					value="entity.chargingFciId" data-placeholder="Select FCI..." cssClass="chosen-select"
					emptyOption="true" cssStyle="width:350px;" name="entity.chargingFciId"/>
				</td>
			</tr>
            <tr>
				<td><s:text name="profiles.fciservid" /></td>
				<td><s:textfield key="entity.chargingFciServIdent" /></td>
			</tr>
            <tr>
				<td><s:text name="profiles.sciid" /></td>
				<td><s:select label="profiles.sciid" list="sciList" listKey="sci.id" listValue="optionString"
					value="entity.chargingSciId" data-placeholder="Select tariff definition..." cssClass="chosen-select"
					emptyOption="true" cssStyle="width:350px;" name="entity.chargingSciId"/>
				</td>
			</tr>
		</tbody>
	</table>
	<s:submit value="%{getText('operation.save')}"
		action="saveNewProfile" />
	<s:submit value="%{getText('operation.cancel')}" action="listProfiles" />
</s:form>