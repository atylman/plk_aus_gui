<%@ include file="/WEB-INF/content/taglibs.jsp"%>

<br />

<s:form>
	<s:text name="profiles.search" />
	<s:textfield key="queryPattern" />
	<s:submit value="Search" action="listProfiles" />
</s:form>

<table class="elements-list" style="width: 400px">
	<thead>
		<tr>
			<th class="table-title"><s:text name="profiles.list" /> <s:if
					test="incompleteSearch">
					<span> <s:text name="search.narrow.msg">
							<s:param>
								<s:property value="maxSearchResults" />
							</s:param>
							<s:param>
								<s:property value="totalMatching" />
							</s:param>
						</s:text>
					</span>
				</s:if></th>
		</tr>
		<tr>
			<th><s:text name="profiles.id" /></th>
		</tr>
	</thead>
	<tbody>
		<s:iterator var="settings" value="entityList">
			<tr>
				<td>
				    <s:a action="profileDetails">
						<s:param name="id">
							<s:property value="msisdn" />
						</s:param>
						<s:property value="msisdn" />
					</s:a>
                </td>
			</tr>
		</s:iterator>
	</tbody>
</table>
<common:ifUserInRole roles="ADMIN">
	<div class="formControls">
		<s:a action="newProfile">
			<s:text name="operation.new" />
		</s:a>
	</div>
</common:ifUserInRole>