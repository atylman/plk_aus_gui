<%@ include file="/WEB-INF/content/taglibs.jsp"%>

<br />

<s:form>
	<s:text name="prefixes.search" />
	<s:textfield key="queryPattern" />
	<s:submit value="Search" action="listPrefixes" />
</s:form>

<table class="elements-list" style="width: 400px">
	<thead>
		<tr>
			<th colspan="2" class="table-title"><s:text name="prefixes.list" /> <s:if
					test="incompleteSearch">
					<span> <s:text name="search.narrow.msg">
							<s:param>
								<s:property value="maxSearchResults" />
							</s:param>
							<s:param>
								<s:property value="totalMatching" />
							</s:param>
						</s:text>
					</span>
				</s:if></th>
		</tr>
		<tr>
			<th><s:text name="prefixes.id" /></th>
			<th><s:text name="prefixes.description" /></th>
		</tr>
	</thead>
	<tbody>
		<s:iterator var="settings" value="entityList">
			<tr>
				<td>
				    <s:a action="prefixDetails">
						<s:param name="id">
							<s:property value="prefixNumber"/>
						</s:param>
						<s:property value="prefixNumber" />
					</s:a>
				</td>
				<td>
					<s:property value="description" />
				</td>
			</tr>
		</s:iterator>
	</tbody>
</table>
<common:ifUserInRole roles="ADMIN">
	<div class="formControls">
		<s:a action="newPrefix">
			<s:text name="operation.new" />
		</s:a>
	</div>
</common:ifUserInRole>