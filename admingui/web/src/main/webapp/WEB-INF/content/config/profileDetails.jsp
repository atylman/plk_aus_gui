<%@ include file="/WEB-INF/content/taglibs.jsp"%>

<table class="elements-list" style="width: 670px">
	<thead>
		<tr>
			<th colspan="2" class="table-title">
			<s:text name="profiles.details" /></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 200px"><s:text name="profiles.id" /></td>
			<td><s:property value="entity.msisdn" /></td>
		</tr>
		<tr>
			<td><s:text name="profiles.isblocked" /></td>
			<td><s:property value="entity.isBlockedString" /></td>
		</tr>
		<tr>
			<td><s:text name="prefixes.id" /></td>
			<td><s:property value="entity.prefixNumber" /></td>
		</tr>
		<tr>
			<td><s:text name="profiles.annid" /></td>
			<td><s:property value="entity.announcementId" /></td>
		</tr>
		<tr>
			<td><s:text name="profiles.fciid" /></td>
			<td><s:property value="entity.chargingFciId" /></td>
		</tr>
		<tr>
			<td><s:text name="profiles.fciservid" /></td>
			<td><s:property value="entity.chargingFciServIdent" /></td>
		</tr>
		<tr>
			<td><s:text name="profiles.sciid" /></td>
			<td><s:property value="entity.chargingSciId" /></td>
		</tr>
	</tbody>
</table>
<div class="formControls">
	<common:ifUserInRole roles="ADMIN">
		<s:a action="editProfile">
			<s:text name="operation.modify" />
			<s:param name="id" value="entity.msisdn" />
		</s:a>
    &nbsp;|&nbsp;
    <s:a action="deleteProfile" cssClass="delete">
			<s:text name="operation.delete" />
			<s:param name="id" value="entity.msisdn" />
		</s:a>
    &nbsp;|&nbsp;
  </common:ifUserInRole>
	<s:a action="listProfiles">
		<s:text name="operation.back" />
	</s:a>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		$('.delete').bind('click', function(event) {
			event.preventDefault();
			if (confirm('<s:text name="delete.confirm.message"/>')) {
				document.location = $(this).attr('href');
			}
		});
	});
</script>