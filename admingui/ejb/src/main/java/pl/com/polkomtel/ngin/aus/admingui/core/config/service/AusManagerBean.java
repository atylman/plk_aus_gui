package pl.com.polkomtel.ngin.aus.admingui.core.config.service;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanFci;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanFciManager;
import com.nsn.ploc.feniks.admingui.configuration.provider.MBeanPropertiesProvider;
import com.nsn.ploc.feniks.admingui.core.commons.announ.manager.AnnounManager;
import com.nsn.ploc.feniks.admingui.core.commons.tariff.manager.TariffDefManager;
import com.nsn.ploc.feniks.admingui.core.commons.tariff.model.TariffDef;
import com.nsn.ploc.feniks.admingui.core.profiles.model.Service;
import com.nsn.ploc.feniks.admingui.core.profiles.service.ServiceManager;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.LocalBinding;
import pl.com.polkomtel.ngin.aus.admingui.core.config.model.*;
import pl.com.polkomtel.ngin.aus.admingui.core.config.util.ValidationError;
import com.nsn.ploc.feniks.admingui.core.commons.announ.model.Announcement;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

@Stateless
@LocalBinding(jndiBinding = AusManager.LOCAL_JNDI_NAME)
public class AusManagerBean implements AusManager {
    private static final String COMMONS_PROPERTIES_FILE = "commons";
    private static final String LDAP_HOST_PROPERTY = "cupr.host";
    private static final String LDAP_PORT_PROPERTY = "cupr.port";
    private static final String LDAP_USER_PROPERTY = "cupr.user";
    private static final String LDAP_PASSWORD_PROPERTY = "cupr.password";
    private static final String AUSPROFILES_LDAP_PATH = "ou=profiles,ou=aus,ou=ngin,ou=services";
    private static final String PREFIXES_LDAP_PATH = "ou=prefixes,ou=aus,ou=ngin,ou=services";
    private static final String MSISDN_PREFIX = "msisdn=";
    private static final String PREFIXNUMBER_PREFIX = "prefixNumber=";
    private static final Log LOGGER = LogFactory.getLog(AusManagerBean.class);
    private static final String SERVICE_NAME = "AUS";
    private MBeanPropertiesProvider propertiesProvider;
    private String ldapHost;
    private String ldapPort;
    private String ldapUser;
    private String ldapPass;

    @EJB(mappedName = AnnounManager.JNDI_NAME)
    protected AnnounManager announcementManager;

    @EJB(mappedName = TariffDefManager.JNDI_NAME)
    protected TariffDefManager sciManager;

    @EJB(mappedName = UanFciManager.REMOTE_JNDI_NAME)
    protected UanFciManager fciManager;

    @EJB(mappedName = ServiceManager.REMOTE_JNDI_NAME)
    protected ServiceManager serviceManager;


    @PostConstruct
    public void init() {
        this.propertiesProvider = new MBeanPropertiesProvider(COMMONS_PROPERTIES_FILE);
        this.ldapHost = propertiesProvider.getProperties().get(LDAP_HOST_PROPERTY);
        this.ldapPort = propertiesProvider.getProperties().get(LDAP_PORT_PROPERTY);
        this.ldapUser = propertiesProvider.getProperties().get(LDAP_USER_PROPERTY);
        this.ldapPass = propertiesProvider.getProperties().get(LDAP_PASSWORD_PROPERTY);
    }

    protected DirContext getCuprConnection() throws NamingException {
        Hashtable<String, String> env = new Hashtable<String, String>(); // NOSONAR
        // since it's interface for external library and we can't change it
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");

        String ldapAddress = "ldap://" + this.ldapHost + ":" + this.ldapPort + "/o=polkomtel,dc=cupr";
        env.put(Context.PROVIDER_URL, ldapAddress);

        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, this.ldapUser);
        env.put(Context.SECURITY_CREDENTIALS, this.ldapPass);

        return new InitialDirContext(env);
    }

    @Override
    public List<AusProfile> listAusProfiles(String ausProfileQueryPattern) throws NamingException {
        DirContext ctx = getCuprConnection();
        String query;
        SearchControls ctls = new SearchControls();
        if (StringUtils.isEmpty(ausProfileQueryPattern)) {
            query = "*";
        } else {
            query = ausProfileQueryPattern;
        }
        String filter = "(" + MSISDN_PREFIX + query + ")";
        NamingEnumeration<SearchResult> ne = ctx.search(AUSPROFILES_LDAP_PATH,
                filter, ctls);

        List<AusProfile> ausProfiles = new ArrayList<AusProfile>();

        while (ne.hasMore()) {
            SearchResult elem = ne.next();
            // Since the results are returned in msisdn=x format, we strip the msisdn=
            // part to get just the msisdn
            AusProfile ausProfile = new AusProfile(elem.getAttributes());
            ausProfiles.add(ausProfile);
        }
        Collections.sort(ausProfiles);

        return ausProfiles;
    }

    @Override
    public AusProfile detailsAusProfile(String msisdn) throws NamingException {
        DirContext ctx = getCuprConnection();
        String ausProfileLdapPath = MSISDN_PREFIX + msisdn + "," + AUSPROFILES_LDAP_PATH;
        Attributes ausProfileLdapAttributes = ctx.getAttributes(ausProfileLdapPath);
        return new AusProfile(ausProfileLdapAttributes);
    }

    @Override
    public void addAusProfile(AusProfile entity) throws NamingException {
        DirContext ctx = getCuprConnection();
        LOGGER.debug("adding AusProfile " + entity);
        String ausProfileLdapPath = MSISDN_PREFIX + entity.getMsisdn() + ","
                + AUSPROFILES_LDAP_PATH;
        Attributes ausProfileLdapAttributes = entity.getAttributes();
        ctx.createSubcontext(ausProfileLdapPath, ausProfileLdapAttributes);
    }

    @Override
    public void validateAusProfile(AusProfile entity) throws ValidationError { // NOSONAR
        // all basic sanity checks, sonar tries to complain it's not simple enough, but all the checks are
        // readable
        if (StringUtils.isEmpty(entity.getMsisdn())) {
            throw new ValidationError("MSISDN can't be empty!");
        }
        if (entity.getIsBlocked() && StringUtils.isEmpty(entity.getAnnouncementId())) {
            throw new ValidationError("Cannot define a blocked profile with no announcement!");
        }
        if (!entity.getIsBlocked() && StringUtils.isEmpty(entity.getPrefixNumber())) {
            throw new ValidationError("Cannot define an unblocked profile with no prefix number!");
        }
        if (!entity.getIsBlocked() && StringUtils.isNotEmpty(entity.getPrefixNumber()) &&
                StringUtils.isNotEmpty(entity.getChargingFciId()) &&
                StringUtils.isEmpty(entity.getChargingFciServIdent())) {
            throw new ValidationError("Cannot define an unblocked profile with" +
                    " a prefix number and FciId but no FciServiceID!");
        }
        if (StringUtils.isNotEmpty(entity.getPrefixNumber())) {
            checkPrefixExists(entity.getPrefixNumber());
        }
    }

    private void checkPrefixExists(String prefix) throws ValidationError {
        try {
            List<Prefix> prefixList = listPrefixes("*");
            boolean prefixFound = false;
            for (Prefix p : prefixList) {
                if (p.getPrefixNumber().equals(prefix)) {
                    prefixFound = true;
                }
            }
            if (!prefixFound) {
                throw new ValidationError("Given prefix '" + prefix +
                        "' doesn't exist in the database. Create it first.");
            }
        } catch (NamingException e) {
           LOGGER.error("Error while trying to list prefixes for prefix existance checking", e);
        }
    }

    @Override
    public void deleteAusProfile(String msisdn) throws NamingException {
        DirContext ctx = getCuprConnection();
        String ausProfileLdapPath = "msisdn=" + msisdn + "," + AUSPROFILES_LDAP_PATH;
        ctx.destroySubcontext(ausProfileLdapPath);
    }

    @Override
    public void editAusProfile(String msisdn, AusProfile ausProfile) throws NamingException {
        // the ID is being changed
        if (!msisdn.equals(ausProfile.getMsisdn())) {
            // ID change is not supported
            return;
        }
        DirContext ctx = getCuprConnection();
        String ausProfileLdapPath = MSISDN_PREFIX + msisdn + "," + AUSPROFILES_LDAP_PATH;
        List<ModificationItem> modList = new ArrayList<ModificationItem>();
        modList.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                new BasicAttribute("isBlocked",
                        ausProfile.getIsBlockedString())));
        Attributes origAttributes = ctx.getAttributes(ausProfileLdapPath);
        addIfNotNull(modList, modifyOptionalAttribute(origAttributes, "prefixNumber",
                ausProfile.getPrefixNumber()));
        addIfNotNull(modList, modifyOptionalAttribute(origAttributes, "announcementId",
                ausProfile.getAnnouncementId()));
        addIfNotNull(modList, modifyOptionalAttribute(origAttributes, "chargingFciId",
                ausProfile.getChargingFciId()));
        addIfNotNull(modList, modifyOptionalAttribute(origAttributes, "chargingSciId",
                ausProfile.getChargingSciId()));
        addIfNotNull(modList, modifyOptionalAttribute(origAttributes, "chargingFciServIdent",
                ausProfile.getChargingFciServIdent()));
        ModificationItem[] mods = new ModificationItem[modList.size()];
        mods = modList.toArray(mods);
        ctx.modifyAttributes(ausProfileLdapPath, mods);
    }

    @Override
    public List<Prefix> listPrefixes(String prefixQueryPattern) throws NamingException {
        DirContext ctx = getCuprConnection();

        SearchControls ctls = new SearchControls();
        String query;
        if (StringUtils.isEmpty(prefixQueryPattern)) {
            query = "*";
        } else {
            query = prefixQueryPattern;
        }
        String filter = "(" + PREFIXNUMBER_PREFIX + query + ")";
        NamingEnumeration ne = ctx.search(PREFIXES_LDAP_PATH, filter, ctls);

        List<Prefix> prefixes = new ArrayList<Prefix>();

        while (ne.hasMore()) {
            SearchResult elem = (SearchResult) ne.next();
            Prefix prefix = new Prefix(elem.getAttributes());
            prefixes.add(prefix);
        }
        Collections.sort(prefixes);

        ctx.close();
        return prefixes;
    }

    @Override
    public Prefix detailsPrefix(String prefixNumber) throws NamingException {
        DirContext ctx = getCuprConnection();
        String prefixLdapPath = PREFIXNUMBER_PREFIX + prefixNumber + "," + PREFIXES_LDAP_PATH;
        Attributes prefixLdapAttributes = ctx.getAttributes(prefixLdapPath);
        return new Prefix(prefixLdapAttributes);
    }

    @Override
    public void addPrefix(Prefix entity) throws NamingException {
        DirContext ctx = getCuprConnection();
        String prefixLdapPath = PREFIXNUMBER_PREFIX + entity.getPrefixNumber() + ","
                + PREFIXES_LDAP_PATH;
        Attributes prefixLdapAttributes = entity.getAttributes();
        ctx.createSubcontext(prefixLdapPath, prefixLdapAttributes);
    }

    @Override
    public void validatePrefix(Prefix entity) throws ValidationError {
        if (StringUtils.isEmpty(entity.getPrefixNumber())) {
            throw new ValidationError("Prefix can't be empty!");
        } else if (StringUtils.isEmpty(entity.getDescription())) {
        throw new ValidationError("Description field can't be empty!");
        }
    }

    @Override
    public void deletePrefix(String prefixNumber) throws NamingException {
        DirContext ctx = getCuprConnection();
        String prefixLdapPath = PREFIXNUMBER_PREFIX + prefixNumber + "," + PREFIXES_LDAP_PATH;
        ctx.destroySubcontext(prefixLdapPath);
    }

    @Override
    public void canDeletePrefix(String prefixNumber) throws ValidationError {
        List<AusProfile> profiles = null;
        try {
            profiles = listAusProfiles("*");
            for (AusProfile ap : profiles) {
                if (prefixNumber.equals(ap.getPrefixNumber())) {
                    throw new ValidationError("Can't delete prefix '" + prefixNumber +
                            "' because a profile [MSISDN: " + ap.getMsisdn() + "] is using it.");
                }
            }
        } catch (NamingException e) {
            LOGGER.error("Error while trying to retrieve list of profiles " +
                    "for checking deletion of prefix ", e);
        }
    }

    @Override
    public void editPrefix(String prefixNumber, Prefix prefix) throws NamingException {
        // the ID is being changed
        if (!prefixNumber.equals(prefix.getPrefixNumber())) {
            // ID change is not supported
            return;
        }
        DirContext ctx = getCuprConnection();
        String prefixLdapPath = PREFIXNUMBER_PREFIX + prefixNumber + "," + PREFIXES_LDAP_PATH;
        List<ModificationItem> modList = new ArrayList<ModificationItem>();
        modList.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                new BasicAttribute("description",
                        prefix.getDescription())));
        ModificationItem[] mods = new ModificationItem[modList.size()];
        mods = modList.toArray(mods);
        ctx.modifyAttributes(prefixLdapPath, mods);
    }

    @Override
    public String getDefaultFciServIdent() {
        Service service = serviceManager.getServiceByName(SERVICE_NAME);
        return service.getServiceIdentifier();
    }

    private static <T> void addIfNotNull(List<T> list, T elem) {
        if (elem != null) {
            list.add(elem);
        }
    }

    private ModificationItem modifyOptionalAttribute(Attributes origAttributes,
                                                     String attrName, String newValue) throws NamingException {
        Attribute origAttr = findAttribute(origAttributes, attrName);
        if (origAttr == null && newValue.isEmpty()) {
            return null;
        } else if (origAttr == null) {
            return new ModificationItem(DirContext.ADD_ATTRIBUTE,
                    new BasicAttribute(attrName, newValue));
        } else if (newValue.isEmpty()) {
            return new ModificationItem(DirContext.REMOVE_ATTRIBUTE,
                    new BasicAttribute(attrName, origAttr.get()));
        } else {
            return new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                    new BasicAttribute(attrName, newValue));
        }
    }

    private Attribute findAttribute(Attributes attributes, String attrName) throws NamingException {
        for (NamingEnumeration<?> ne = attributes.getAll(); ne.hasMore();) {
            Attribute attr = (Attribute) ne.next();
            String attrId = attr.getID();
            if (attrId.equals(attrName)) {
                return attr;
            }
        }
        return null;
    }

    @Override
    public List<AnnWrapper> getAllAnnouncements() {
        List<Announcement> origList = announcementManager.getAll();
        List<AnnWrapper> wrapList = new ArrayList<AnnWrapper>();
        for (Announcement ann : origList) {
            wrapList.add(new AnnWrapper(ann));
        }
        return wrapList;
    }

    @Override
    public List<SciWrapper> getAllSci() {
        List<TariffDef> origList = sciManager.getAll();
        List<SciWrapper> wrapList = new ArrayList<SciWrapper>();
        for (TariffDef sci : origList) {
            wrapList.add(new SciWrapper(sci));
        }
        return wrapList;
    }

    @Override
    public List<FciWrapper> getAllFci() {
        List<UanFci> origList = fciManager.getAll();
        List<FciWrapper> wrapList = new ArrayList<FciWrapper>();
        for (UanFci fci : origList) {
            wrapList.add(new FciWrapper(fci));
        }
        return wrapList;
    }


}
