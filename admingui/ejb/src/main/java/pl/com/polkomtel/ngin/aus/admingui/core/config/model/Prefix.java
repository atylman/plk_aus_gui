package pl.com.polkomtel.ngin.aus.admingui.core.config.model;


import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;

public class Prefix implements Comparable<Prefix> {
    private String prefixNumber;
    private String description;

    public Prefix() {
        prefixNumber = "";
        description = "";
    }

    public Prefix(Attributes prefixLdapAttributes)
            throws NamingException {
        this();
        for (NamingEnumeration<?> ae = prefixLdapAttributes.getAll(); ae.hasMore();) {
            Attribute attr = (Attribute) ae.next();
            parseAttribute(attr);
        }
    }

    private void parseAttribute(Attribute attr) throws NamingException {
        String attrId = attr.getID();
        if ("prefixNumber".equals(attrId)) {
            setPrefixNumber((String) attr.get());
        } else if ("description".equals(attrId)) {
            setDescription((String) attr.get());
        }
    }

    public Attributes getAttributes() {
        Attributes attributes = new BasicAttributes();
        Attribute objectClass = new BasicAttribute("objectClass", "top");
        // mandatory attributes
        objectClass.add("prefix");
        attributes.put(objectClass);
        attributes.put("prefixNumber", getPrefixNumber());
        // optional
        if (!getDescription().isEmpty()) {
            attributes.put("description", getDescription());
        }
        return attributes;
    }

    @Override
    public String toString() {
        return "Prefix [prefixNumber=" + prefixNumber +
                ", description=" + description +
                ']';
    }

    public String getPrefixNumber() {
        return prefixNumber;
    }

    public void setPrefixNumber(String prefixNumber) {
        this.prefixNumber = prefixNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOptionString() {
        return this.getPrefixNumber() + " (" + this.description + ")";
    }

    @Override
    public int compareTo(Prefix p) { // NOSONAR
        return this.prefixNumber.compareTo(p.getPrefixNumber());
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Prefix)) {
            return false;
        } else if (obj == this) {
            return true;
        }
        return this.prefixNumber.equals(((Prefix) obj).getPrefixNumber());
    }

    public int hashCode() {
        return 0;
    }
}
