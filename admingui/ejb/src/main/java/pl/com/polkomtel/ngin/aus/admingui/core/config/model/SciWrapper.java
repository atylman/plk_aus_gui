package pl.com.polkomtel.ngin.aus.admingui.core.config.model;

import com.nsn.ploc.feniks.admingui.core.commons.tariff.model.TariffDef;

public class SciWrapper {

    TariffDef sci;

    public SciWrapper(TariffDef td) {
        this.sci = td;
    }

    public TariffDef getSci() {
        return sci;
    }

    public void setSci(TariffDef sci) {
        this.sci = sci;
    }

    public String getOptionString() {
        return this.sci.getId() + " " + this.sci.getName();
    }

}
