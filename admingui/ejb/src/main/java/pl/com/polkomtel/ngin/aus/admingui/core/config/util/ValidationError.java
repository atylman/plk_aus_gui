package pl.com.polkomtel.ngin.aus.admingui.core.config.util;

public class ValidationError extends Exception {
    private static final long serialVersionUID = 1L;

    public ValidationError(String ex) {
        super(ex);
    }

}
