package pl.com.polkomtel.ngin.aus.admingui.core.config.model;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;

public class AusProfile implements Comparable<AusProfile> {
    private String msisdn;
    private String prefixNumber;
    private Boolean isBlocked;
    private String announcementId;
    private String chargingFciId;
    private String chargingSciId;
    private String chargingFciServIdent;

    public AusProfile() {
        msisdn = "";
        prefixNumber = "";
        isBlocked = false;
        announcementId = "";
        chargingFciId = "";
        chargingSciId = "";
        chargingFciServIdent = "";
    }

    public AusProfile(Attributes ausProfileLdapAttributes)
            throws NamingException {
        this();
        for (NamingEnumeration<?> ae = ausProfileLdapAttributes.getAll(); ae.hasMore();) {
            Attribute attr = (Attribute) ae.next();
            parseAttribute(attr);
        }
    }

    @Override
    public String toString() {
        return "AusProfile [msisdn=" + msisdn +
                ", prefixNumber=" + prefixNumber +
                ", isBlocked=" + getIsBlockedString() +
                ", announcementId=" + announcementId +
                ", chargingFciId=" + chargingFciId +
                ", chargingFciServIdent=" + chargingFciServIdent +
                ", chargingSciId=" + chargingSciId +
                ']';
    }


    private void parseAttribute(Attribute attr) throws NamingException {
        String attrId = attr.getID();
        if ("msisdn".equals(attrId)) {
            setMsisdn((String) attr.get());
        } else if ("prefixNumber".equals(attrId)) {
            setPrefixNumber((String) attr.get());
        } else if ("announcementId".equals(attrId)) {
            setAnnouncementId((String) attr.get());
        } else if ("chargingFciId".equals(attrId)) {
            setChargingFciId((String) attr.get());
        } else if ("chargingFciServIdent".equals(attrId)) {
            setChargingFciServIdent((String) attr.get());
        } else if ("chargingSciId".equals(attrId)) {
            setChargingSciId((String) attr.get());
        } else if ("isBlocked".equals(attrId)) {
            String ldapIsBlocked = (String) attr.get();
            if ("TRUE".equals(ldapIsBlocked)) {
                setIsBlocked(true);
            } else {
                setIsBlocked(false);
            }
        }
    }

    public Attributes getAttributes() {
        Attributes attributes = new BasicAttributes();
        Attribute objectClass = new BasicAttribute("objectClass", "top");
        // mandatory attributes
        objectClass.add("ausProfile");
        attributes.put(objectClass);
        attributes.put("msisdn", getMsisdn());
        attributes.put("isBlocked", getIsBlockedString());
        // optional
        if (!getAnnouncementId().isEmpty()) {
            attributes.put("announcementId", getAnnouncementId());
        }
        if (!getPrefixNumber().isEmpty()) {
            attributes.put("prefixNumber", getPrefixNumber());
        }
        if (!getChargingFciId().isEmpty()) {
            attributes.put("chargingFciId", getChargingFciId());
        }
        if (!getChargingFciServIdent().isEmpty()) {
            attributes.put("chargingFciServIdent", getChargingFciServIdent());
        }
        if (!getChargingSciId().isEmpty()) {
            attributes.put("chargingSciId", getChargingSciId());
        }
        return attributes;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPrefixNumber() {
        return prefixNumber;
    }

    public void setPrefixNumber(String prefixNumber) {
        this.prefixNumber = prefixNumber;
    }

    public Boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    public String getIsBlockedString() {
        if (getIsBlocked()) {
            return "TRUE";
        } else {
            return "FALSE";
        }
    }

    public String getAnnouncementId() {
        return announcementId;
    }

    public void setAnnouncementId(String announcementId) {
        this.announcementId = announcementId;
    }

    public String getChargingFciId() {
        return chargingFciId;
    }

    public void setChargingFciId(String chargingFciId) {
        this.chargingFciId = chargingFciId;
    }

    public String getChargingSciId() {
        return chargingSciId;
    }

    public void setChargingSciId(String chargingSciId) {
        this.chargingSciId = chargingSciId;
    }

    public String getChargingFciServIdent() {
        return chargingFciServIdent;
    }

    public void setChargingFciServIdent(String chargingFciServIdent) {
        this.chargingFciServIdent = chargingFciServIdent;
    }

    @Override
    public int compareTo(AusProfile ap) { //NOSONAR
        return this.msisdn.compareTo(ap.getMsisdn());
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AusProfile)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.msisdn.equals(((AusProfile) obj).getMsisdn());
    }

    public int hashCode() {
        return 0;
    }

}
