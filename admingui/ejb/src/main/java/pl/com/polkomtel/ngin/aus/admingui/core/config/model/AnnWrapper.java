package pl.com.polkomtel.ngin.aus.admingui.core.config.model;

import com.nsn.ploc.feniks.admingui.core.commons.announ.model.Announcement;

public class AnnWrapper {

    Announcement announcement;

    public AnnWrapper(Announcement ann) {
        this.announcement = ann;
    }

    public String getOptionString() {
        return announcement.getId() + " " + announcement.getName();
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }
}
