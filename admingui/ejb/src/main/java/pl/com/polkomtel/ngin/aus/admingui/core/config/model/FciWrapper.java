package pl.com.polkomtel.ngin.aus.admingui.core.config.model;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanFci;

public class FciWrapper {

    UanFci fci;

    public FciWrapper(UanFci td) {
        this.fci = td;
    }

    public UanFci getFci() {
        return fci;
    }

    public void setFci(UanFci fci) {
        this.fci = fci;
    }

    public String getOptionString() {
        return this.fci.getFciId() + ", charged: " + this.fci.getChargedParty();
    }

}
