package pl.com.polkomtel.ngin.aus.admingui.core.config.service;

import pl.com.polkomtel.ngin.aus.admingui.core.config.model.*;
import pl.com.polkomtel.ngin.aus.admingui.core.config.util.ValidationError;

import javax.ejb.Local;
import javax.naming.NamingException;
import java.util.List;

@Local
public interface AusManager {
    public static final String LOCAL_JNDI_NAME = "aus/AusManager";

    public List<AusProfile> listAusProfiles(String ausProfileQueryPattern) throws NamingException;

    public AusProfile detailsAusProfile(String msisdn) throws NamingException;

    public void addAusProfile(AusProfile entity) throws NamingException;

    public void validateAusProfile(AusProfile entity) throws ValidationError;

    public void deleteAusProfile(String msisdn) throws NamingException;

    public void editAusProfile(String msisdn, AusProfile ausProfile) throws NamingException;

    public List<Prefix> listPrefixes(String prefixQueryPattern) throws NamingException;

    public Prefix detailsPrefix(String prefixNumber) throws NamingException;

    public void addPrefix(Prefix entity) throws NamingException;

    public void validatePrefix(Prefix entity) throws ValidationError;

    public void deletePrefix(String prefixNumber) throws NamingException;

    void canDeletePrefix(String prefixNumber) throws ValidationError;

    public void editPrefix(String prefixNumber, Prefix prefix) throws NamingException;

    public String getDefaultFciServIdent();

    List<AnnWrapper> getAllAnnouncements();

    List<SciWrapper> getAllSci();

    List<FciWrapper> getAllFci();
}
