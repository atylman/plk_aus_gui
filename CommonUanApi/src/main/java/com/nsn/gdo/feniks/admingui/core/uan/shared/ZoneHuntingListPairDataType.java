package com.nsn.gdo.feniks.admingui.core.uan.shared;

import com.nsn.ploc.feniks.admingui.core.commons.lists.model.LocationListSet;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanHuntingList;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingNumber;

public class ZoneHuntingListPairDataType {

	private UanHuntingList uanHuntingList;
	
	private UanRoutingNumber uanRoutingNumber;
	
	private String huntingListName;

	private LocationListSet locationListSet;
	
	private long locationMatchType;

	public UanHuntingList getUanHuntingList() {
		return uanHuntingList;
	}

	public void setUanHuntingList(UanHuntingList uanHuntingList) {
		this.uanHuntingList = uanHuntingList;
	}

	public UanRoutingNumber getUanRoutingNumber() {
		return uanRoutingNumber;
	}

	public void setUanRoutingNumber(UanRoutingNumber uanRoutingNumber) {
		this.uanRoutingNumber = uanRoutingNumber;
	}

	public String getHuntingListName() {
		return huntingListName;
	}

	public void setHuntingListName(String huntingListName) {
		this.huntingListName = huntingListName;
	}

	public LocationListSet getLocationListSet() {
		return locationListSet;
	}

	public void setLocationListSet(LocationListSet locationListSet) {
		this.locationListSet = locationListSet;
	}

	public long getLocationMatchType() {
		return locationMatchType;
	}

	public void setLocationMatchType(long locationMatchType) {
		this.locationMatchType = locationMatchType;
	}
	
	@Override
	public String toString() {
		
		return "ZoneHuntingListPairDataType [uanHuntingList=" + uanHuntingList+ ", huntingListName=" + huntingListName
		+ ", locationListSet=" + locationListSet + ", locationMatchType=" + locationMatchType + ", uanRoutingNumber=" + uanRoutingNumber
			+ "]";
	}
}
