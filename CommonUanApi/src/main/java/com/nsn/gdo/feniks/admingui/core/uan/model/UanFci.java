package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

@Entity
@Table(name="UAN_FCI", schema=UanConstants.SCHEMA_UAN)
public class UanFci implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @SequenceGenerator(name="FCI_ID_GENERATOR", sequenceName="FCI_ID_SQ", allocationSize=1, initialValue=1)
	@GeneratedValue(generator="FCI_ID_GENERATOR")
	@Column(name="FCI_ID", unique=true, nullable=false)	
	Long fciId;
	
	@Column(name="CHARGED_PARTY")
	Long chargedParty;
	
	@Column(name="EXT_NUM")
	String extNum;
	
	@Column(name="EXT_TON")
	Long extTon;
	
	@Column(name="PCT")
	Long pct;

	@OneToOne
	@JoinColumn(name="SUBSCRIPTION_ID")
	private ServiceSubscription serviceSubscription;
	
	public Long getFciId() {
		return fciId;
	}

	public void setFciId(Long fciId) {
		this.fciId = fciId;
	}

	public Long getChargedParty() {
		return chargedParty;
	}

	public void setChargedParty(Long chargedParty) {
		this.chargedParty = chargedParty;
	}

	public String getExtNum() {
		return extNum;
	}

	public void setExtNum(String extNum) {
		this.extNum = extNum;
	}

	public Long getExtTon() {
		return extTon;
	}

	public void setExtTon(Long extTon) {
		this.extTon = extTon;
	}

	public Long getPct() {
		return pct;
	}

	public void setPct(Long pct) {
		this.pct = pct;
	}

	public ServiceSubscription getServiceSubscription() {
		return serviceSubscription;
	}

	public void setServiceSubscription(ServiceSubscription serviceSubscription) {
		this.serviceSubscription = serviceSubscription;
	}

	@Override
	public String toString() {
		return "UanFci [fciId=" + fciId + ", chargedParty=" + chargedParty
				+ ", extNum=" + extNum + ", extTon=" + extTon + ", pct=" + pct
				+ "]";
	}

	@PrePersist 
    @PreUpdate
    private void toUpperCase() {
          
          if(getExtNum() != null)
          {
        	  extNum = getExtNum().toUpperCase();
          }
        
    }

}
