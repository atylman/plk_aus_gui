package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.nsn.ploc.feniks.admingui.core.commons.announ.model.Announcement;
import com.nsn.ploc.feniks.admingui.core.commons.causecodes.model.CauseCode;
import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

@Entity
@Table(name="UAN_SCREENING", schema=UanConstants.SCHEMA_UAN)
public class UanScreening implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @SequenceGenerator(name="SCREENING_PROFILE_ID_GENERATOR", sequenceName="SCREENING_ID_SQ", allocationSize=1, initialValue=1)
	@GeneratedValue(generator="SCREENING_PROFILE_ID_GENERATOR")
	@Column(name="SCREENING_PROFILE_ID", unique=true, nullable=false)
	Long screeningProfileId;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="CRIT_ID")	
	UanCriterion critId;
	
	@Column(name="MATCH_TREATMENT")
	Long matchTreatment;
	
	@Column(name="ON_BARRED_ACTION")
	Long onBarredAction;
	
	@Column(name="ON_ALLOWED_ACTION")
	Long onAllowedAction;
	
	@Column(name="CS_ID_FOR_BARRED")
	Long csIdForBarred;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="RN_ID_FOR_BARRED")
	UanRoutingNumber rnIdForBarred;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ANN_ID_FOR_BARRED")
	Announcement annIdForBarred;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="DISCONNECT_CAUSE")
	CauseCode  disconnectCause;
	
	@Column(name="CS_ID_FOR_ALLOW")
	Long csIdForAllow;
	
	@OneToOne
	@JoinColumn(name="SUBSCRIPTION_ID")
	private ServiceSubscription serviceSubscription;

	
	public Long getScreeningProfileId() {
		return screeningProfileId;
	}

	public void setScreeningProfileId(Long screeningProfileId) {
		this.screeningProfileId = screeningProfileId;
	}

	public UanCriterion getCritId() {
		return critId;
	}

	public void setCritId(UanCriterion critId) {
		this.critId = critId;
	}

	public Long getMatchTreatment() {
		return matchTreatment;
	}

	public void setMatchTreatment(Long matchTreatment) {
		this.matchTreatment = matchTreatment;
	}

	public Long getOnBarredAction() {
		return onBarredAction;
	}

	public void setOnBarredAction(Long onBarredAction) {
		this.onBarredAction = onBarredAction;
	}

	public Long getOnAllowedAction() {
		return onAllowedAction;
	}

	public void setOnAllowedAction(Long onAllowedAction) {
		this.onAllowedAction = onAllowedAction;
	}

	public Long getCsIdForBarred() {
		return csIdForBarred;
	}

	public void setCsIdForBarred(Long csIdForBarred) {
		this.csIdForBarred = csIdForBarred;
	}

	public UanRoutingNumber getRnIdForBarred() {
		return rnIdForBarred;
	}

	public void setRnIdForBarred(UanRoutingNumber rnIdForBarred) {
		this.rnIdForBarred = rnIdForBarred;
	}

	public Announcement getAnnIdForBarred() {
		return annIdForBarred;
	}

	public void setAnnIdForBarred(Announcement annIdForBarred) {
		this.annIdForBarred = annIdForBarred;
	}

	public CauseCode getDisconnectCause() {
		return disconnectCause;
	}

	public void setDisconnectCause(CauseCode disconnectCause) {
		this.disconnectCause = disconnectCause;
	}


	public ServiceSubscription getServiceSubscription() {
		return serviceSubscription;
	}

	public void setServiceSubscription(ServiceSubscription serviceSubscription) {
		this.serviceSubscription = serviceSubscription;
	}

	@Override
	public String toString() {
		return "UanScreening [screeningProfileId=" + screeningProfileId
				+ ", critId=" + critId + ", matchTreatment=" + matchTreatment
				+ ", onBarredAction=" + onBarredAction + ", onAllowedAction="
				+ onAllowedAction + ", csIdForBarred=" + csIdForBarred
				+ ", rnIdForBarred=" + rnIdForBarred + ", annIdForBarred="
				+ annIdForBarred + ", disconnectCause=" + disconnectCause
				+ ", csIdForAllow=" + csIdForAllow + ", serviceSubscription="
				+ serviceSubscription + "]";
	}




}
