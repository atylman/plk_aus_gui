package com.nsn.gdo.feniks.admingui.core.uan.service;

import java.math.BigDecimal;

import javax.ejb.Remote;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingConfiguration;


@Remote
public interface UanRoutingConfigurationManager {

	public static final String REMOTE_JNDI_NAME  = "uan/UanRoutingConfigurationManagerRemote";
	
	public UanRoutingConfiguration getRoutingConfigById(Long rrId);
	
	public UanRoutingConfiguration getRoutingConfigBySubsId(Long subsId);
	
	public BigDecimal getRoutingConfigNextSequence();

}
