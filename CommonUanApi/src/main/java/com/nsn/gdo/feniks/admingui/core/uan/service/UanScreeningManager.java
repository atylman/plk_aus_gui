package com.nsn.gdo.feniks.admingui.core.uan.service;

import javax.ejb.Remote;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanScreening;


@Remote
public interface UanScreeningManager {

	public static final String REMOTE_JNDI_NAME  = "uan/UanScreeningManagerRemote";
	
	public UanScreening getScreeningByProfileId(Long screeningProfileId);
	
}
