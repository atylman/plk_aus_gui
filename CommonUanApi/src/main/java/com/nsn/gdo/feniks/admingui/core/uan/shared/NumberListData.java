package com.nsn.gdo.feniks.admingui.core.uan.shared;


public class NumberListData {
	//Long key;
	Long id;
	//String value;
	String name;
	
	public NumberListData() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public NumberListData(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return "NumberListData [id=" + id + ", name=" + name + "]";
	}
	
	/*public NumberListData(Long key, String value) {
		super();
		this.key = key;
		this.value = value;
	}
	
	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "NumberListData [key=" + key + ", value=" + value + "]";
	}*/
	
	
	
	
}
