package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.nsn.ploc.feniks.admingui.core.commons.causecodes.model.CauseCode;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

@Entity
@Table(name="UAN_DEFAULT_TREATMENT", schema=UanConstants.SCHEMA_UAN)
public class UanDefault implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @SequenceGenerator(name="DEFAULT_TREATMENT_ID_GENERATOR", sequenceName="DF_TREATMENT_ID_SEQ", allocationSize=1, initialValue=1)
	@GeneratedValue(generator="DEFAULT_TREATMENT_ID_GENERATOR")
	@Column(name="DEFAULT_TREATMENT_ID", unique=true, nullable=false)	
	Long defaultTreatmentID;
	
	@Column(name="DEFAULT_TREATMENT")
	Long defaultTreatment;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.MERGE)
	@JoinColumn(name="RELEASE_CODE")
	CauseCode releaseCode;
	
	@Column(name="ANN_ID")
	Long annID;

	
	public Long getDefaultTreatmentID() {
		return defaultTreatmentID;
	}

	public void setDefaultTreatmentID(Long defaultTreatmentID) {
		this.defaultTreatmentID = defaultTreatmentID;
	}

	public Long getDefaultTreatment() {
		return defaultTreatment;
	}

	public void setDefaultTreatment(Long defaultTreatment) {
		this.defaultTreatment = defaultTreatment;
	}

	public CauseCode getReleaseCode() {
		return releaseCode;
	}

	public void setReleaseCode(CauseCode releaseCode) {
		this.releaseCode = releaseCode;
	}

	public Long getAnnID() {
		return annID;
	}

	public void setAnnID(Long annID) {
		this.annID = annID;
	}

	@Override
	public String toString() {
		return "UanDefault [defaultTreatmentID=" + defaultTreatmentID
				+ ", defaultTreatment=" + defaultTreatment + ", releaseCode="
				+ releaseCode + ", annID=" + annID + "]";
	}
	
	
}
