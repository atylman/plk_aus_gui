package com.nsn.gdo.feniks.admingui.core.uan.shared;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanIvrCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingNumber;

public class FreePhoneSubscriptionDao {

	UanIvrCriterion uanIvrCriterion;
	UanRoutingNumber uanRoutingNumber;
	
	public UanIvrCriterion getUanIvrCriterion() {
		return uanIvrCriterion;
	}
	public void setUanIvrCriterion(UanIvrCriterion uanIvrCriterion) {
		this.uanIvrCriterion = uanIvrCriterion;
	}
	public UanRoutingNumber getUanRoutingNumber() {
		return uanRoutingNumber;
	}
	public void setUanRoutingNumber(UanRoutingNumber uanRoutingNumber) {
		this.uanRoutingNumber = uanRoutingNumber;
	}
	@Override
	public String toString() {
		return "FreePhoneSubscriptionDao [uanIvrCriterion=" + uanIvrCriterion
				+ ", uanRoutingNumber=" + uanRoutingNumber + "]";
	}
	
}

