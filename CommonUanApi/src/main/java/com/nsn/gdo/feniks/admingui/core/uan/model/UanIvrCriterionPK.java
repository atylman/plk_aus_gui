package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UanIvrCriterionPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="IVR_CRIT_ID", nullable=false)	
	Long ivrCritId;	
	
	@Column(name="CHOICE", nullable=false)
	String choice;

	public Long getIvrCritId() {
		return ivrCritId;
	}

	public void setIvrCritId(Long ivrCritId) {
		this.ivrCritId = ivrCritId;
	}

	public String getChoice() {
		return choice;
	}

	public void setChoice(String choice) {
		this.choice = choice;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((choice == null) ? 0 : choice.hashCode());
		result = prime * result
				+ ((ivrCritId == null) ? 0 : ivrCritId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UanIvrCriterionPK))
			return false;
		UanIvrCriterionPK other = (UanIvrCriterionPK) obj;
		if (choice == null) {
			if (other.choice != null)
				return false;
		} else if (!choice.equals(other.choice))
			return false;
		if (ivrCritId == null) {
			if (other.ivrCritId != null)
				return false;
		} else if (!ivrCritId.equals(other.ivrCritId))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "UanIvrCriterionPK [ivrCritId=" + ivrCritId + ", choice="
				+ choice + "]";
	}
	
}
