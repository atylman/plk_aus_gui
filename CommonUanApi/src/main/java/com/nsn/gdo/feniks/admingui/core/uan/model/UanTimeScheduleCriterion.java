package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.nsn.ploc.feniks.admingui.core.commons.lists.model.TimescheduleListSet;
import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

@Entity
@Table(name="UAN_TIME_SCHEDULE_CRITERION", schema=UanConstants.SCHEMA_UAN)
public class UanTimeScheduleCriterion extends CriteriaType implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="TOD_CRIT_ID", unique=true, nullable=false)	
	Long todCritId;
	
	@Column(name="MATCH_TYPE")
	Long matchType;
	
    @ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="TIME_GLOBAL_LIST")
	private TimescheduleListSet timescheduleListSet;

	@OneToOne
	@JoinColumn(name="SUBSCRIPTION_ID")
	private ServiceSubscription serviceSubscription;
	
	public Long getTodCritId() {
		return todCritId;
	}

	public void setTodCritId(Long todCritId) {
		this.todCritId = todCritId;
	}

	public Long getMatchType() {
		return matchType;
	}

	public void setMatchType(Long matchType) {
		this.matchType = matchType;
	}

	public TimescheduleListSet getTimescheduleListSet() {
		return timescheduleListSet;
	}

	public void setTimescheduleListSet(TimescheduleListSet timescheduleListSet) {
		this.timescheduleListSet = timescheduleListSet;
	}

	public boolean isCustomTimescheduleSet() {
		return (timescheduleListSet != null && timescheduleListSet.isCustomSet()) ? true : false;
	}
	
	public ServiceSubscription getServiceSubscription() {
		return serviceSubscription;
	}

	public void setServiceSubscription(ServiceSubscription serviceSubscription) {
		this.serviceSubscription = serviceSubscription;
	}

	
	public boolean isNoneTimescheduleListSet() {
		return (timescheduleListSet != null && timescheduleListSet.getSetId().intValue() == UanConstants.NoneListSetValue.intValue()) ? true : false;
	}
	
	@Override
	public String toString() {
		return "UanTimeScheduleCriterion [todCritId=" + todCritId
				+ ", matchType=" + matchType + ", timescheduleListSet="
				+ timescheduleListSet + "]";
	}

	
}
