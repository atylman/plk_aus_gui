package com.nsn.gdo.feniks.admingui.core.uan.shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingNumber;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanZoneCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;
import com.nsn.ploc.feniks.admingui.core.commons.lists.model.ListCommons;
import com.nsn.ploc.feniks.admingui.core.commons.lists.model.LocationList;
import com.nsn.ploc.feniks.admingui.core.commons.lists.model.LocationListSet;

public class PolcardIntranetSubscriptionDao {
	
	UanRoutingNumber uanRoutingNumber;
	UanZoneCriterion uanZoneCriterion;
	List<LocationListData> lists = new ArrayList<LocationListData>();
	
	private static final String EMPTY_JSON_ARRAY = "[]";
	protected static final String STRING_ID_SEPARATOR = "#:#";
	protected static final String NAME_ID_SEPARATOR = "@:@";
	
	public UanRoutingNumber getUanRoutingNumber() {
		return uanRoutingNumber;
	}

	public void setUanRoutingNumber(UanRoutingNumber uanRoutingNumber) {
		this.uanRoutingNumber = uanRoutingNumber;
	}

	public UanZoneCriterion getUanZoneCriterion() {
		return uanZoneCriterion;
	}

	public void setUanZoneCriterion(UanZoneCriterion uanZoneCriterion) {
		this.uanZoneCriterion = uanZoneCriterion;
	}

	public List<LocationListData> getLists() {
		return lists;
	}

	public void setLists(List<LocationListData> lists) {
		this.lists = lists;
	}

	public LocationListSet getUpdatedSet() {
		if(uanZoneCriterion.getMatchType().intValue() == UanConstants.UanZoneMatchingOption.ZONE_GLOBAL_LIST.getValue()){
			LocationListSet set = new LocationListSet();
			if(getLists() != null && getLists().size() != 0) {	
				set.setSetId(null);
				set.setLists(new HashSet<LocationList>());
				List<Long> result = new ArrayList<Long>();
				for (LocationListData list : getLists()) {
						result.add(list.getId());
					}
				for(Long listId : result) {
					LocationList tsList = new LocationList();
					tsList.setId(listId);
					set.getLists().add(tsList);
				}
			}
			return set;
		}else if (uanZoneCriterion.getMatchType().intValue() == UanConstants.UanZoneMatchingOption.ANY.getValue()){
			return getAnySet();
		} else {
			return null;
		}
	}	
	
	public void setListData(LocationListSet set) {
		if(set != null && set.getLists() != null) {
			for (ListCommons list : set.getLists()) {
				lists.add(new LocationListData(list.getId(), list.getName()));				
			}
		}
	}
	
	public boolean isSelectionValid() {
		if(lists.isEmpty()) {
			return false;
		}
		for (LocationListData list : lists) {	
			if(list.getId() == null) {
				return false;
			}
		}
		return true;
	}
	
	private LocationListSet getAnySet() {
		LocationListSet set = new LocationListSet();
		set.setSetId(1L);
		LocationList list = new LocationList();
		list.setId(1L);
		list.setName("ANY");
		set.setLists(Collections.singleton(list));
		return set;
	}

	public String getListsAsJSON() {
		if(uanZoneCriterion.getMatchType().intValue() == UanConstants.UanZoneMatchingOption.ZONE_GLOBAL_LIST.getValue()) {
			return new Gson().toJson(getLists());
		} else {
			return EMPTY_JSON_ARRAY;
		}
	}

	public void setLists(String stringForm) {
		lists = new ArrayList<LocationListData>();
		String[] idsArray = stringForm.split(STRING_ID_SEPARATOR);
		for (String id : idsArray) {
			String[] pair = id.split(NAME_ID_SEPARATOR);
			if(pair.length == 2) {
				lists.add(new LocationListData(new Long(pair[0]), pair[1]));
			}
		}
	}	
	
	public String getListsNamesAsString() {
		StringBuffer sb = new StringBuffer("[");
		List<String> tmp = new ArrayList<String>();
		for (LocationListData list : getLists()) {
			tmp.add(list.name);
		}
		if(tmp.size() > 0) {
			Collections.sort(tmp);
			sb.append(StringUtils.join(tmp, ", "));
		} else {
			sb.append(" ");
		}
		sb.append("]");
		return sb.toString();
	}
	
	@Override
	public String toString() {
		return "PolcardIntranetSubscriptionDao [uanRoutingNumber="
				+ uanRoutingNumber + ", uanZoneCriterion=" + uanZoneCriterion
				+ ", lists=" + lists + "]";
	}

}
