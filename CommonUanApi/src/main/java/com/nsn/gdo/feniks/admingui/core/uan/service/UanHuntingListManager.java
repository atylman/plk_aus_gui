package com.nsn.gdo.feniks.admingui.core.uan.service;

import java.math.BigDecimal;

import javax.ejb.Remote;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanHuntingList;
import com.nsn.ploc.feniks.admingui.exception.ApplicationStorageException;

@Remote
public interface UanHuntingListManager {
	
	public static final String REMOTE_JNDI_NAME  = "uan/UanHuntingListManager";
	
	public UanHuntingList getHuntingList(Long hlId);
	
	public BigDecimal getNextSequence();

	public void removeHuntingList(Long hlId) throws ApplicationStorageException;
	
}
