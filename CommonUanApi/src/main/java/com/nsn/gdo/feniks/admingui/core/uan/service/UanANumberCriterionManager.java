package com.nsn.gdo.feniks.admingui.core.uan.service;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanANumberCriterion;

@Remote
public interface UanANumberCriterionManager {

	public static final String REMOTE_JNDI_NAME = "uan/UanANumberCriterionManagerRemote";
	
	public Long getANumberCriteriaIdForSubscrId(Long subscriptionId);
	
	public UanANumberCriterion getANumberCriteriaForSubscrId(Long subscriptionId);
	
	public BigDecimal getANumberCriterionNextSequence();
	
	public UanANumberCriterion getANumberCriterionByCritId(Long critId);
	
	public List<Long> getAllANumCritIdForSubscription(Long subscriptionId);
	
}
