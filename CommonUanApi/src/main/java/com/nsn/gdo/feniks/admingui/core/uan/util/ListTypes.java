package com.nsn.gdo.feniks.admingui.core.uan.util;

import java.util.ArrayList;
import java.util.List;

import com.nsn.gdo.feniks.admingui.core.uan.model.KeyValue;


public class ListTypes {

	public static final String CONTINUE = "CONTINUE";
	public static final String RELEASE = "RELEASE";

	public static final String B_NUM = "B Num";
	public static final String C_NUM = "C Num";
	public static final String EXTERNAL_NUM = "External Num";
	
	private List<KeyValue<Long, String>> defaultTreatmentTypes;
	private List<KeyValue<Long, String>> defaultBillAddrTypes;
	

	public ListTypes() {
		
		defaultTreatmentTypes = new ArrayList<KeyValue<Long,String>>();
		defaultTreatmentTypes.add(new KeyValue<Long, String>(
				Long.valueOf(0), CONTINUE));
		defaultTreatmentTypes.add(new KeyValue<Long, String>(
				Long.valueOf(1), RELEASE));
		
		defaultBillAddrTypes = new ArrayList<KeyValue<Long,String>>();
		defaultBillAddrTypes.add(new KeyValue<Long, String>(
				Long.valueOf(0), B_NUM));
		defaultBillAddrTypes.add(new KeyValue<Long, String>(
				Long.valueOf(1), C_NUM));
		defaultBillAddrTypes.add(new KeyValue<Long, String>(
				Long.valueOf(2), EXTERNAL_NUM));		
	}

	public List<KeyValue<Long, String>> getDefaultTreatmentTypes() {
		return defaultTreatmentTypes;
	}

	public void setDefaultTreatmentTypes(List<KeyValue<Long, String>> defaultTreatmentTypes) {
		this.defaultTreatmentTypes = defaultTreatmentTypes;
	}
	
	public static boolean isTypeContinue(String type) {
		return isType(type, CONTINUE);
	}
	
	public static boolean isTypeRelease(String type) {
		return isType(type, RELEASE);
	}

	public List<KeyValue<Long, String>> getDefaultBillAddrTypes() {
		return defaultBillAddrTypes;
	}

	public void setDefaultBillAddrTypes(
			List<KeyValue<Long, String>> defaultBillAddrTypes) {
		this.defaultBillAddrTypes = defaultBillAddrTypes;
	}	

	public static boolean isTypeBNum(String type) {
		return isType(type, B_NUM);
	}
	
	public static boolean isTypeCNum(String type) {
		return isType(type, C_NUM);
	}
	
	public static boolean isTypeExternalNum(String type) {
		return isType(type, EXTERNAL_NUM);
	}

	private static boolean isType(String type, String typeName) {
		if(typeName.equals(type)) {
			return true;
		}
		return false;
	}
	
}
