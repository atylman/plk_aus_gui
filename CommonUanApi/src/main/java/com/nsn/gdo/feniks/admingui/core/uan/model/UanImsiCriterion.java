package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

@Entity
@Table(name="UAN_IMSI_CRITERION", schema=UanConstants.SCHEMA_UAN)
public class UanImsiCriterion implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private UanImsiCriterionPK id;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="RN_ID")
	private UanRoutingNumber rnId;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="SCROAM_INFO")
	private UanScroamInfo scroamInfo;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.MERGE)
	@JoinColumn(name="SUBSCRIPTION_ID")
	private ServiceSubscription subscriptionId;

	public UanImsiCriterionPK getId() {
		return id;
	}

	public void setId(UanImsiCriterionPK id) {
		this.id = id;
	}

	public UanRoutingNumber getRnId() {
		return rnId;
	}

	public void setRnId(UanRoutingNumber rnId) {
		this.rnId = rnId;
	}

	public UanScroamInfo getScroamInfo() {
		return scroamInfo;
	}

	public void setScroamInfo(UanScroamInfo scroamInfo) {
		this.scroamInfo = scroamInfo;
	}

	public ServiceSubscription getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(ServiceSubscription subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	@Override
	public String toString() {
		return "UanImsiCriterion [id=" + id + ", rnId=" + rnId + ", scroamInfo="
				+ scroamInfo + ", subscriptionId=" + subscriptionId + "]";
	}
}
