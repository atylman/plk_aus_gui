package com.nsn.gdo.feniks.admingui.core.uan.service;

import java.util.List;

import javax.ejb.Remote;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanSubscription;


@Remote
public interface UanSubscriptionManager {

	public static final String REMOTE_JNDI_NAME  = "uan/UanSubscriptionManagerRemote";
	
	public UanSubscription getSubscriptionById(Long id);
	
	@SuppressWarnings("rawtypes")
	public List getSubscriptionAsListById(Long id);
	
}
