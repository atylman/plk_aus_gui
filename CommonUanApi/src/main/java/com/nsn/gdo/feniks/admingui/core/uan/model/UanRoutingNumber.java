package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.nsn.ploc.feniks.admingui.core.commons.announ.model.Announcement;
import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

@Entity
@Table(name="UAN_ROUTING_NUMBER", schema=UanConstants.SCHEMA_UAN)
public class UanRoutingNumber implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @SequenceGenerator(name="RN_ID_GENERATOR", sequenceName="RN_ID_SQ", allocationSize=1, initialValue=1)
	@GeneratedValue(generator="RN_ID_GENERATOR")
	@Column(name="RN_ID", unique=true, nullable=false)
	Long rnID;
	
	@Column(name="RN_TYPE")
	Long rnType;
	
	@Column(name="DIR_NUMBER")
	String dirNum;
	
	@Column(name="TON")
	Long ton;
	
	@Column(name="HL_ID")
	Long hlID;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ANN_ID")
	Announcement annID;
    
	@OneToOne
	@JoinColumn(name="SUBSCRIPTION_ID")
	private ServiceSubscription serviceSubscription;
	
	@Column(name="Max_hunting_attempts")
	private Long maxHuntingAttempts;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "FCI_ID", nullable=true)
	UanFci fci;
	
	public Long getRnID() {
		return rnID;
	}

	public void setRnID(Long rnID) {
		this.rnID = rnID;
	}

	public Long getRnType() {
		return rnType;
	}

	public void setRnType(Long rnType) {
		this.rnType = rnType;
	}

	public String getDirNum() {
		return dirNum;
	}

	public void setDirNum(String dirNum) {
		this.dirNum = dirNum;
	}

	public Long getTon() {
		return ton;
	}

	public void setTon(Long ton) {
		this.ton = ton;
	}

	public Long getHlID() {
		return hlID;
	}

	public void setHlID(Long hlID) {
		this.hlID = hlID;
	}

	public Announcement getAnnID() {
		return annID;
	}

	public void setAnnID(Announcement annID) {
		this.annID = annID;
	}

	public ServiceSubscription getServiceSubscription() {
		return serviceSubscription;
	}

	public void setServiceSubscription(ServiceSubscription serviceSubscription) {
		this.serviceSubscription = serviceSubscription;
	}

	@Override
	public String toString() {
		return "UanRoutingNumber [rnID=" + rnID + ", rnType=" + rnType
				+ ", dirNum=" + dirNum + ", ton=" + ton + ", hlID=" + hlID
				+ ", annID=" + annID + ", maxHuntingAttempts=" + maxHuntingAttempts +  ", uanFci=" + fci + "]";
	}

	public Long getMaxHuntingAttempts() {
		return maxHuntingAttempts;
	}

	public void setMaxHuntingAttempts(Long maxHuntingAttempts) {
		this.maxHuntingAttempts = maxHuntingAttempts;
	}

	@PrePersist 
    @PreUpdate
    private void toUpperCase() {
         
          if(getDirNum() != null)
          {
        	  dirNum = getDirNum().toUpperCase();
          }
       }
	
	public UanFci getFci() {
		return fci;
	}

	public void setFci(UanFci fci) {
		this.fci = fci;
	}
}
