package com.nsn.gdo.feniks.admingui.core.uan.util;

import java.util.ArrayList;
import java.util.List;

import com.nsn.gdo.feniks.admingui.core.uan.model.KeyValue;


public class UanConstants {
		
	public static final String PERSISTENCE_UNIT_NAME="UANPU";
	
	public static final String SCHEMA_UAN="UAN";

	public static final Long NoneListSetValue = 0l;
	public static final Long AnyListSetValue = 1l;
	
	public static final Long TIME_SCHEDULE_NONE_ID = 1L;
	public static final Long TIME_SCHEDULE_CUSTOM_ID = 2L;
	public static final Long TIME_SCHEDULE_ANY_ID = 3L;
	
	
	public enum UanDefaultTreatment
	{
		CONTINUE (0), 
		RELEASE (1) , 
		RELEASE_WITH_ANN (2) , 
		CONNECT_TO_DEFAULT (3); 
		
		private UanDefaultTreatment(int i)
        {
            this.value = i;
        }
		
		private int value;

		public int getValue() {
			return value;
		}
	}
	
	public enum UanCentrumCallCompletionAction
	{
		RE_ROUTE (0), 
		CONTINUE (1) , 
		RELEASE (2) ; 
		
		private UanCentrumCallCompletionAction(int i)
        {
            this.value = i;
        }
		
		private int value;

		public int getValue() {
			return value;
		}
	}

	
	public enum UanCriterionCriteriaTypes
	{
		IVR (1),
		A_NUMBER (2),
		TIME_OF_DAY (3),
		ZONE (4),
		BEARER (5),
		IMSI (6);
		
		private UanCriterionCriteriaTypes(int i)
        {
            this.value = i;
        }
		
		private int value;

		public int getValue() {
			return value;
		}

	}
	
	
	public enum UanIvrCriteriaTranslationType
	{
		ROUTING_CRITEION_AGAIN (0), 
		ROUTING_NUMBER (1); 
		
		private UanIvrCriteriaTranslationType(int i)
        {
            this.value = i;
        }
		
		private int value;

		public int getValue() {
			return value;
		}
	
	}
	
	public enum UanRoutingNumberRNType
	{
		DIRECT_NUMBER (0), 
		HUNTING_LIST (1), 
		ANNOUNCEMENT (2),
		GLOBAL_LIST (3); 
		
		private UanRoutingNumberRNType(int i)
        {
            this.value = i;
        }
		
		private int value;

		public int getValue() {
			return value;
		}
	
	}
	
	public enum UanRoutingConfigRRType
	{
		DEFAULT_ROUTING (0), 
		CRITERION_BASED_ROUTING (1);
		
		private UanRoutingConfigRRType(int i)
        {
            this.value = i;
        }
		
		private int value;

		public int getValue() {
			return value;
		}
	
	}
	
	
	public enum UanRoutingConfigOnMatchAction
	{
		CHAINED (0), 
		ROUTING (1),
		DISCONNECT (2);
		
		private UanRoutingConfigOnMatchAction(int i)
        {
            this.value = i;
        }
		
		private int value;

		public int getValue() {
			return value;
		}
	
	}
	
	
	public enum UanRoutingConfigOnNOMatchAction
	{
		CHAINED (0), 
		ROUTING (1),
		DISCONNECT (2),
		CONTINUE (3);
		
		private UanRoutingConfigOnNOMatchAction(int i)
        {
            this.value = i;
        }
		
		private int value;

		public int getValue() {
			return value;
		}
	
	}	
	
	
	public enum UanScreeningOnBarredAction
	{
		CONTINUE_ON_BARRED (3), 
		CONNECT_ON_BARRED (5), 
		DISCONNECT_ON_BARRED (6),
		PLAY_ANN_AND_DISCONNECT (7),
		SEND_CONTINUE_ON_BARRED (12);
		
		private UanScreeningOnBarredAction(int i)
        {
            this.value = i;
        }	
		
		private int value;

		public int getValue() {
			return value;
		}
		
	}
	
	
	public enum UanScreeningOnAllowedAction
	{
		CONTINUE_ON_ALLOWED (2), 
		CONNECT_ON_BARRED (4);

		
		private UanScreeningOnAllowedAction(int i)
        {
            this.value = i;
        }	
		
		private int value;

		public int getValue() {
			return value;
		}
	
	}
	
	public enum UanEnableDisableFlag
	{
		DISABLE (0),
		ENABLE (1);
				
		private UanEnableDisableFlag(int i)
        {
            this.value = i;
        }	
		
		private int value;

		public int getValue() {
			return value;
		}
	

	}
	
	
	public enum UanScreeningMatchTreatment
	{
		ALLOW_CRITERIA_MATCHED (1), 
		BARRED_CRITERIA_NO_MATCHED (2);

		
		private UanScreeningMatchTreatment(int i)
        {
            this.value = i;
        }	
		
		private int value;

		public int getValue() {
			return value;
		}
	
	}
	
	
	public enum UanFciChargedParty
	{
		B_PARTY (1), 
		C_PARTY (2), 
		EXTERNAL (3);
		
		private UanFciChargedParty(int i)
        {
            this.value = i;
        }	
		
		private int value;

		public int getValue() {
			return value;
		}
	
	}
	
	
	public enum UanTON
	{
		International (4),
		National (3),
		Unknown(2),
		Subscriber (1);
		
		private UanTON(int i)
        {
            this.value = i;
        }	
		
		private int value;

		public int getValue() {
			return value;
		}

	}
	
	
	public enum UanTimeSchedCriteriaMatchType
	{
		PERIOD (1), 
		TIME_RANGE_GLOBAL_LIST (2), 
		ANY (3);
		
		private UanTimeSchedCriteriaMatchType(int i)
        {
            this.value = i;
        }	
		
		private int value;

		public int getValue() {
			return value;
		}
		

	}
	
	public enum UanANumberCriteriaMatchingOption
	{
		DIRECT_NUMBER (1), 
		PREFIX (2), 
		NUMBER_LIST (3),
		ANY(4),
		UNKNOWN(5);
		
		private UanANumberCriteriaMatchingOption(int i)
        {
            this.value = i;
        }	
		
		private int value;

		public int getValue() {
			return value;
		}
		
	
	}
	
	public enum UanANumberCriteriaNumToMatch
	{
		CALLING_ADDRESS (1), 
		REDIRECTION_ADDRESS (2), 
		CALLED_ADDRESS (3),
		ANY(4);
		
		private UanANumberCriteriaNumToMatch(int i)
        {
            this.value = i;
        }	
		
		private int value;

		public int getValue() {
			return value;
		}
		

	}
	
	public enum UanZoneCritZoneInfo
	{
		CELL_ID (1), 
		VLR (0);
				
		private UanZoneCritZoneInfo(int i)
        {
            this.value = i;
        }	
		
		private int value;

		public int getValue() {
			return value;
		}
	
	}
	
	public enum UanZoneMatchingOption
	{
		ZONE_GLOBAL_LIST (1),
		ANY (2);
				
		private UanZoneMatchingOption(int i)
        {
            this.value = i;
        }	
		
		private int value;

		public int getValue() {
			return value;
		}
	
	}
	
	
	public static List<KeyValue<Long,String>> getTonTypes()
	{
		List<KeyValue<Long,String>> tonTypes = new ArrayList<KeyValue<Long,String>>();
		UanTON[] tons = UanTON.values();
		for(UanTON uanTON : tons)
		{
			tonTypes.add(new KeyValue<Long, String>(
					new Long(uanTON.value), uanTON.name()));
		}
		return tonTypes;
	}
}
