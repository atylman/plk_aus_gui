package com.nsn.gdo.feniks.admingui.core.uan.service;

import java.math.BigDecimal;

import javax.ejb.Remote;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanTimeScheduleCriterion;


@Remote
public interface UanTimeScheduleCriterionManager {

	public static final String REMOTE_JNDI_NAME = "uan/UanTimeScheduleCriterionManagerRemote";
	
	public UanTimeScheduleCriterion getTimeScheduleCriteriaForSubscrId(Long subscriptionId);
	
	public Long getTimeScheduleCriteriaIdForSubscrId(Long subscriptionId);
	
	public BigDecimal getTimeSchdCriterionNextSequence();

	public UanTimeScheduleCriterion getTimeScheduleCriteriaFromRoutingConfig(
			Long subscriptionId);

	public Long getTimeScheduleCriteriaIdFromRoutingConfig(Long subscriptionId);
	
}
