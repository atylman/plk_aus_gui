package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.nsn.ploc.feniks.admingui.core.commons.announ.model.Announcement;
import com.nsn.ploc.feniks.admingui.core.commons.causecodes.model.CauseCode;
import com.nsn.ploc.feniks.admingui.core.commons.tariff.model.TariffDef;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

@Entity
@Table(name = "UAN_SERVICE_TYPE", schema = UanConstants.SCHEMA_UAN)
public class UanServiceType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SERVICE_TYPE")
	private Long serviceType;

	@Column(name = "PREM_ENT_BAR_FLAG")
	Long premEntBarFlag;

	@Column(name = "PREM_INFO_BAR_FLAG")
	Long premInfoBarFlag;

	@Column(name = "SCREENING_FLAG")
	Long screeningFlag;

	@Column(name = "REROUTING_FLAG")
	Long reroutingFlag;

	@Column(name = "ANN_FLAG")
	Long annFlag;

	@Column(name = "CHARGING_FLAG")
	Long chargingFlag;

	@Column(name = "FCI_FLAG")
	Long fciFlag;

	@Column(name = "FCI_PARTY_FLAG")
	Long fciPartyFlag;

	@Column(name = "FCI_PCT_FLAG")
	Long fciPctFlag;

	@Column(name = "SCI_FLAG")
	Long sciFlag;

	@Column(name = "MAX_HUNTING_ATTEMPTS")
	Long maxHuntingAttempts;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "DEFAULT_HANDLING_ID_ERROR")
	UanDefault defaultHandlingIDError;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DEFAULT_HANDLING_ID_CONF")
	UanDefault defaultHandlingIDSub;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "DEFAULT_HANDLING_ID_VIDEO")
	UanDefault defaultHandlingIDVideo;

	@Column(name = "DEFAULT_HANDLING_ID_HL")
	Long defaultHandlingIDHuntinglist;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "DEFAULT_HANDLING_ID")
	UanDefault defaultHandlingId;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="RELEASE_CODE_ODB")
	CauseCode releaseCauseODB;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "RELEASE_CODE_BEARER")
	CauseCode releaseCauseBearer;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DEFAULT_SCI_REF")
	TariffDef defaultSciRef;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DEFAULT_WELCOME_ANN_REF")
	Announcement defaultWelcomeAnn;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SCREENING_RELEASE_CAUSE")
	CauseCode screeningReleaseCause;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name="DEFAULT_ANUM_CRIT_ID")    
    UanCriterion anumCritId;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="CALL_COMPLETION_RELEASE_CAUSE")
	CauseCode callCompletionReleaseCause;	
	
	public Long getServiceType() {
		return serviceType;
	}

	public void setServiceType(Long serviceType) {
		this.serviceType = serviceType;
	}

	public Long getPremEntBarFlag() {
		return premEntBarFlag;
	}

	public void setPremEntBarFlag(Long premEntBarFlag) {
		this.premEntBarFlag = premEntBarFlag;
	}

	public Long getPremInfoBarFlag() {
		return premInfoBarFlag;
	}

	public void setPremInfoBarFlag(Long premInfoBarFlag) {
		this.premInfoBarFlag = premInfoBarFlag;
	}

	public Long getScreeningFlag() {
		return screeningFlag;
	}

	public void setScreeningFlag(Long screeningFlag) {
		this.screeningFlag = screeningFlag;
	}

	public Long getReroutingFlag() {
		return reroutingFlag;
	}

	public void setReroutingFlag(Long reroutingFlag) {
		this.reroutingFlag = reroutingFlag;
	}

	public Long getAnnFlag() {
		return annFlag;
	}

	public void setAnnFlag(Long annFlag) {
		this.annFlag = annFlag;
	}

	public Long getChargingFlag() {
		return chargingFlag;
	}

	public void setChargingFlag(Long chargingFlag) {
		this.chargingFlag = chargingFlag;
	}

	public Long getFciFlag() {
		return fciFlag;
	}

	public void setFciFlag(Long fciFlag) {
		this.fciFlag = fciFlag;
	}

	public Long getFciPartyFlag() {
		return fciPartyFlag;
	}

	public void setFciPartyFlag(Long fciPartyFlag) {
		this.fciPartyFlag = fciPartyFlag;
	}

	public Long getFciPctFlag() {
		return fciPctFlag;
	}

	public void setFciPctFlag(Long fciPctFlag) {
		this.fciPctFlag = fciPctFlag;
	}

	public Long getSciFlag() {
		return sciFlag;
	}

	public void setSciFlag(Long sciFlag) {
		this.sciFlag = sciFlag;
	}

	public Long getMaxHuntingAttempts() {
		return maxHuntingAttempts;
	}

	public void setMaxHuntingAttempts(Long maxHuntingAttempts) {
		this.maxHuntingAttempts = maxHuntingAttempts;
	}

	public UanDefault getDefaultHandlingIDError() {
		return defaultHandlingIDError;
	}

	public void setDefaultHandlingIDError(UanDefault defaultHandlingIDError) {
		this.defaultHandlingIDError = defaultHandlingIDError;
	}

	public UanDefault getDefaultHandlingIDSub() {
		return defaultHandlingIDSub;
	}

	public void setDefaultHandlingIDSub(UanDefault defaultHandlingIDSub) {
		this.defaultHandlingIDSub = defaultHandlingIDSub;
	}

	public UanDefault getDefaultHandlingIDVideo() {
		return defaultHandlingIDVideo;
	}

	public void setDefaultHandlingIDVideo(UanDefault defaultHandlingIDVideo) {
		this.defaultHandlingIDVideo = defaultHandlingIDVideo;
	}

	public Long getDefaultHandlingIDHuntinglist() {
		return defaultHandlingIDHuntinglist;
	}

	public void setDefaultHandlingIDHuntinglist(Long defaultHandlingIDHuntinglist) {
		this.defaultHandlingIDHuntinglist = defaultHandlingIDHuntinglist;
	}

	public UanDefault getDefaultHandlingId() {
		return defaultHandlingId;
	}

	public void setDefaultHandlingId(UanDefault defaultHandlingId) {
		this.defaultHandlingId = defaultHandlingId;
	}

	public CauseCode getReleaseCauseODB() {
		return releaseCauseODB;
	}

	public void setReleaseCauseODB(CauseCode releaseCauseODB) {
		this.releaseCauseODB = releaseCauseODB;
	}

	public CauseCode getReleaseCauseBearer() {
		return releaseCauseBearer;
	}

	public void setReleaseCauseBearer(CauseCode releaseCauseBearer) {
		this.releaseCauseBearer = releaseCauseBearer;
	}

	public TariffDef getDefaultSciRef() {
		return defaultSciRef;
	}

	public void setDefaultSciRef(TariffDef defaultSciRef) {
		this.defaultSciRef = defaultSciRef;
	}

	public Announcement getDefaultWelcomeAnn() {
		return defaultWelcomeAnn;
	}

	public void setDefaultWelcomeAnn(Announcement defaultWelcomeAnn) {
		this.defaultWelcomeAnn = defaultWelcomeAnn;
	}


	public CauseCode getScreeningReleaseCause() {
		return screeningReleaseCause;
	}

	public void setScreeningReleaseCause(CauseCode screeningReleaseCause) {
		this.screeningReleaseCause = screeningReleaseCause;
	}
	
	public UanCriterion getAnumCritId() {
		return anumCritId;
	}

	public void setAnumCritId(UanCriterion anumCritId) {
		this.anumCritId = anumCritId;
	}	
	
	public CauseCode getCallCompletionReleaseCause() {
		return callCompletionReleaseCause;
	}

	public void setCallCompletionReleaseCause(CauseCode callCompletionReleaseCause) {
		this.callCompletionReleaseCause = callCompletionReleaseCause;
	}	
	
	@Override
	public String toString() {
		return "UanServiceType [serviceType=" + serviceType
				+ ", premEntBarFlag=" + premEntBarFlag + ", premInfoBarFlag="
				+ premInfoBarFlag + ", screeningFlag=" + screeningFlag
				+ ", reroutingFlag=" + reroutingFlag + ", annFlag=" + annFlag
				+ ", chargingFlag=" + chargingFlag + ", fciFlag=" + fciFlag
				+ ", fciPartyFlag=" + fciPartyFlag + ", fciPctFlag="
				+ fciPctFlag + ", sciFlag=" + sciFlag + ", maxHuntingAttempts="
				+ maxHuntingAttempts + ", defaultHandlingIDError="
				+ defaultHandlingIDError + ", defaultHandlingIDSub="
				+ defaultHandlingIDSub + ", defaultHandlingIDVideo="
				+ defaultHandlingIDVideo + ", defaultHandlingIDHuntinglist="
				+ defaultHandlingIDHuntinglist + ", DefaultHandlingId="
				+ defaultHandlingId + ", releaseCauseODB=" + releaseCauseODB
				+ ", releaseCauseBearer=" + releaseCauseBearer
				+ ", defaultSciRef=" + defaultSciRef + ", anumCritId=" + anumCritId
				+ ", callCompletionReleaseCause=" + callCompletionReleaseCause + "]";
	}

}
