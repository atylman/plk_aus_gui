package com.nsn.gdo.feniks.admingui.core.uan.service;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanIvrCriterion;

@Remote
public interface UanIvrCriterionManager {

	public static final String REMOTE_JNDI_NAME  = "uan/UanIvrCriterionManagerRemote";
		
	public List<UanIvrCriterion> getIvrCriteriaForSubscrId(Long subscriptionId); 
	
	public Long getIvrCriteriaIdForSubscrId(Long subscriptionId);
	
	public BigDecimal getIvrCriterionNextSequence();
	
	public List<UanIvrCriterion> getIvrCriterionList(Long critId);
	
}
