package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UanImsiCriterionPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="IMSI_CRIT_ID", nullable=false)	
	private Long imsiCritId;
	
	@Column(name="IMSI_PREFIX", nullable=false)	
	private String imsiPrefix;

	public Long getImsiCritId() {
		return imsiCritId;
	}

	public void setImsiCritId(Long imsiCritId) {
		this.imsiCritId = imsiCritId;
	}

	public String getImsiPrefix() {
		return imsiPrefix;
	}

	public void setImsiPrefix(String imsiPrefix) {
		this.imsiPrefix = imsiPrefix;
	}

	@Override
	public String toString() {
		return "UanImsiCriterionPK [imsiCritId=" + imsiCritId + ", imsiPrefix="
				+ imsiPrefix + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((imsiCritId == null) ? 0 : imsiCritId.hashCode());
		result = prime * result
				+ ((imsiPrefix == null) ? 0 : imsiPrefix.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UanImsiCriterionPK))
			return false;
		UanImsiCriterionPK other = (UanImsiCriterionPK) obj;
		if (imsiCritId == null) {
			if (other.imsiCritId != null)
				return false;
		} else if (!imsiCritId.equals(other.imsiCritId))
			return false;
		if (imsiPrefix == null) {
			if (other.imsiPrefix != null)
				return false;
		} else if (!imsiPrefix.equals(other.imsiPrefix))
			return false;
		return true;
	}
	
	
}
