package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

@Entity
@Table(name="UAN_CRITERION", schema=UanConstants.SCHEMA_UAN)
public class UanCriterion implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @SequenceGenerator(name="CRITERION_ID_GENERATOR", sequenceName="CRITERION_ID_SQ", allocationSize=1, initialValue=1)
	@GeneratedValue(generator="CRITERION_ID_GENERATOR")
	@Column(name="CRIT_ID", unique=true, nullable=false)	
	Long critId;
		
	@Column(name="CRIT_TYPE")
	Long critType;
	
	@Column(name="CRIT_TYPE_ID")
	Long critTypeId;
	
	@OneToOne
	@JoinColumn(name="SUBSCRIPTION_ID")
	private ServiceSubscription serviceSubscription;
	
	public Long getCritId() {
		return critId;
	}

	public void setCritId(Long critId) {
		this.critId = critId;
	}

	public Long getCritType() {
		return critType;
	}

	public void setCritType(Long critType) {
		this.critType = critType;
	}

	public Long getCritTypeId() {
		return critTypeId;
	}

	public void setCritTypeId(Long critTypeId) {
		this.critTypeId = critTypeId;
	}

	public ServiceSubscription getServiceSubscription() {
		return serviceSubscription;
	}

	public void setServiceSubscription(ServiceSubscription serviceSubscription) {
		this.serviceSubscription = serviceSubscription;
	}

	@Override
	public String toString() {
		return "UanCriterion [critId=" + critId + ", critType=" + critType
				+ ", critTypeId=" + critTypeId + ", serviceSubscription="
				+ serviceSubscription + "]";
	}

	
}