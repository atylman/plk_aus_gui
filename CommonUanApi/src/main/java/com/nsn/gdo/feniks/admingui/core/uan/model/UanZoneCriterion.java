package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.nsn.ploc.feniks.admingui.core.commons.lists.model.LocationListSet;
import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;
import com.nsn.ploc.feniks.admingui.core.commons.lists.model.NumberListSet;

@Entity
@Table(name="UAN_ZONE_CRITERION", schema=UanConstants.SCHEMA_UAN)
public class UanZoneCriterion extends CriteriaType implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	
	@Id
	@Column(name="ZONE_CRIT_ID", unique=true, nullable=false)	
	Long zoneCritId;
	
	@Column(name="MATCH_TYPE")
	Long matchType;
	
	@Column(name="ZONE_INFO")
	Long zoneInfo;
	
    @ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ZONE_INFO_GLOBAL_LIST")
    private LocationListSet locationListSet;

	 @ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="VLR_INFO_GLOBAL_LIST")
    private NumberListSet numberListSet;

	@OneToOne
	@JoinColumn(name="SUBSCRIPTION_ID")
	private ServiceSubscription serviceSubscription;

	public Long getZoneCritId() {
		return zoneCritId;
	}

	public void setZoneCritId(Long zoneCritId) {
		this.zoneCritId = zoneCritId;
	}

	public Long getMatchType() {
		return matchType;
	}

	public void setMatchType(Long matchType) {
		this.matchType = matchType;
	}

	public Long getZoneInfo() {
		return zoneInfo;
	}

	public void setZoneInfo(Long zoneInfo) {
		this.zoneInfo = zoneInfo;
	}

	public LocationListSet getLocationListSet() {
		return locationListSet;
	}

	public void setLocationListSet(LocationListSet locationListSet) {
		this.locationListSet = locationListSet;
	}

	public ServiceSubscription getServiceSubscription() {
		return serviceSubscription;
	}

	public void setServiceSubscription(ServiceSubscription serviceSubscription) {
		this.serviceSubscription = serviceSubscription;
	}
	
	public boolean isCustomLocationListSet() {
		return (locationListSet != null && locationListSet.isCustomSet()) ? true : false;
	}

	public NumberListSet getNumberListSet() {
		return numberListSet;
	}

	public void setNumberListSet(NumberListSet numberListSet) {
		this.numberListSet = numberListSet;
	}

	@Override
	public String toString() {
		return "UanZoneCriterion [zoneCritId=" + zoneCritId + ", matchType="
				+ matchType + ", zoneInfo=" + zoneInfo + ", locationListSet="
				+ locationListSet + ", serviceSubscription="
				+ serviceSubscription + "]";
	}
	
	
	
}
