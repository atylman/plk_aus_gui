package com.nsn.gdo.feniks.admingui.core.uan.shared;


public class LocationListData {
	Long id;
	String name;
	
	public LocationListData() {}
	
	public LocationListData(Long id, String value) {
		super();
		this.id = id;
		this.name = value;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "LocationListData [id=" + id + ", name=" + name + "]";
	}
	
	
}
