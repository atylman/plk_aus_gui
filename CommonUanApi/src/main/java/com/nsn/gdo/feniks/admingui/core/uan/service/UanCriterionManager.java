package com.nsn.gdo.feniks.admingui.core.uan.service;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingConfiguration;

@Remote
public interface UanCriterionManager {

	public static final String REMOTE_JNDI_NAME  = "uan/UanCriterionManagerRemote";
	
	public UanCriterion getCriterionById(Long critId);
	
	public BigDecimal getCriterionNextSequence();
	
	public List<Long> getAllAnumTypeCriterionIdForChainedRouteConfig(UanRoutingConfiguration firstChainedRouteConfigObj);
	
	public List<Long> getAllZoneTypeCriterionIdForChainedRouteConfig(UanRoutingConfiguration firstChainedRouteConfigObj);
	
}
