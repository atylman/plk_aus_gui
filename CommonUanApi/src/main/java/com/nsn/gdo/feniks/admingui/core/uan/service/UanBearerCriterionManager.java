package com.nsn.gdo.feniks.admingui.core.uan.service;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanBearerCriterion;

@Remote
public interface UanBearerCriterionManager {

	public static final String REMOTE_JNDI_NAME  = "uan/UanBearerCriterionManagerRemote";
	
	@SuppressWarnings("rawtypes")
	public List getBearerCriteriaForSubscrId(Long subscriptionId); 
	
	public Long getBearerCriteriaIdForSubscrId(Long subscriptionId);
	
	public BigDecimal getBearerCriterionNextSequence();
	
	public List<UanBearerCriterion> getUanBearerCriterionList(Long critId);

	public enum UanBearerCriterionForTeleservice
	{
		SPEECH (2L), 
		DATA (3L); 
		
		private UanBearerCriterionForTeleservice(Long i)
        {
            this.value = i;
        }
		
		private Long value;

		public Long getValue() {
			return value;
		}
		
		public static String getName(Long i){
			if(i == 0 || i == 2){
				return UanBearerCriterionForTeleservice.values()[i.intValue()].name();
			}
			return ""+i;
		}		
	}
	
	
}
