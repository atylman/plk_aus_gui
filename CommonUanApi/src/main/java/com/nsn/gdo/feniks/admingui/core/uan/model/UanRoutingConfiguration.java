package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.nsn.ploc.feniks.admingui.core.commons.causecodes.model.CauseCode;
import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;


@Entity
@Table(name="UAN_ROUTING_CONFIGURATION", schema=UanConstants.SCHEMA_UAN)
public class UanRoutingConfiguration implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @SequenceGenerator(name="RR_ID_GENERATOR", sequenceName="RR_ID_SQ", allocationSize=1, initialValue=1)
	@GeneratedValue(generator="RR_ID_GENERATOR")
	@Column(name="RR_ID", unique=true, nullable=false)	
	Long rrID;
		
	@Column(name="RR_TYPE")
	Long rrType;
	
    @ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name="DEFAULT_RN_REF")	
	UanRoutingNumber defaultRNRef;
	
    @ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name="CRIT_ID")    
    UanCriterion critID;
	
	@Column(name="ON_MATCH_ACTION")
	Long onMatchAction;
	
	@Column(name="ON_NOMATCH_ACTION")
	Long onNomatchAction;
	
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="CRR_ID_FOR_MATCH")
	UanRoutingConfiguration crrIDForMatch;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="CRN_FOR_MATCH")
	UanRoutingNumber crnForMatch;
	
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="CRR_ID_FOR_NOMATCH")
	UanRoutingConfiguration crrIDForNomatch;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="CRN_FOR_NOMATCH")
	UanRoutingNumber  crnForNomatch;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "RELEASE_CODE_FOR_MATCH")
	CauseCode releaseCodeForMatch;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "RELEASE_CODE_FOR_NOMATCH")
	CauseCode releaseCodeForNomatch;
	
	@OneToOne
	@JoinColumn(name="SUBSCRIPTION_ID")
	private ServiceSubscription serviceSubscription;


	public Long getRrID() {
		return rrID;
	}

	public void setRrID(Long rrID) {
		this.rrID = rrID;
	}

	public Long getRrType() {
		return rrType;
	}

	public void setRrType(Long rrType) {
		this.rrType = rrType;
	}

	public UanRoutingNumber getDefaultRNRef() {
		return defaultRNRef;
	}

	public void setDefaultRNRef(UanRoutingNumber defaultRNRef) {
		this.defaultRNRef = defaultRNRef;
	}

	public UanCriterion getCritID() {
		return critID;
	}

	public void setCritID(UanCriterion critID) {
		this.critID = critID;
	}

	public Long getOnMatchAction() {
		return onMatchAction;
	}

	public void setOnMatchAction(Long onMatchAction) {
		this.onMatchAction = onMatchAction;
	}

	public Long getOnNomatchAction() {
		return onNomatchAction;
	}

	public void setOnNomatchAction(Long onNomatchAction) {
		this.onNomatchAction = onNomatchAction;
	}

	public UanRoutingConfiguration getCrrIDForMatch() {
		return crrIDForMatch;
	}

	public void setCrrIDForMatch(UanRoutingConfiguration crrIDForMatch) {
		this.crrIDForMatch = crrIDForMatch;
	}

	public UanRoutingNumber getCrnForMatch() {
		return crnForMatch;
	}

	public void setCrnForMatch(UanRoutingNumber crnForMatch) {
		this.crnForMatch = crnForMatch;
	}

	public UanRoutingConfiguration getCrrIDForNomatch() {
		return crrIDForNomatch;
	}

	public void setCrrIDForNomatch(UanRoutingConfiguration crrIDForNomatch) {
		this.crrIDForNomatch = crrIDForNomatch;
	}

	public UanRoutingNumber getCrnForNomatch() {
		return crnForNomatch;
	}

	public void setCrnForNomatch(UanRoutingNumber crnForNomatch) {
		this.crnForNomatch = crnForNomatch;
	}

	public CauseCode getReleaseCodeForMatch() {
		return releaseCodeForMatch;
	}

	public void setReleaseCodeForMatch(CauseCode releaseCodeForMatch) {
		this.releaseCodeForMatch = releaseCodeForMatch;
	}

	public CauseCode getReleaseCodeForNomatch() {
		return releaseCodeForNomatch;
	}

	public void setReleaseCodeForNomatch(CauseCode releaseCodeForNomatch) {
		this.releaseCodeForNomatch = releaseCodeForNomatch;
	}

	public ServiceSubscription getServiceSubscription() {
		return serviceSubscription;
	}

	public void setServiceSubscription(ServiceSubscription serviceSubscription) {
		this.serviceSubscription = serviceSubscription;
	}

	@Override
	public String toString() {
		return "UanRoutingConfiguration [rrID=" + rrID + ", rrType=" + rrType
				+ ", defaultRNRef=" + defaultRNRef + ", critID=" + critID
				+ ", onMatchAction=" + onMatchAction + ", onNomatchAction="
				+ onNomatchAction + ", crrIDForMatch=" + crrIDForMatch
				+ ", crnForMatch=" + crnForMatch + ", crrIDForNomatch="
				+ crrIDForNomatch + ", crnForNomatch=" + crnForNomatch
				+ ", releaseCodeForMatch=" + releaseCodeForMatch
				+ ", releaseCodeForNomatch=" + releaseCodeForNomatch + "]";
	}
	
}
