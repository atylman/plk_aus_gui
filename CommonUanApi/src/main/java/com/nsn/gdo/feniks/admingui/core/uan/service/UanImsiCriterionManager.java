package com.nsn.gdo.feniks.admingui.core.uan.service;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

@Remote
public interface UanImsiCriterionManager {

	public static final String REMOTE_JNDI_NAME  = "uan/UanImsiCriterionManager";
	
	@SuppressWarnings("rawtypes")
	public List getImsiCriteriaForSubscrId(Long subscriptionId); 
	
	public Long getImsiCriteriaIdForSubscrId(Long subscriptionId);
	
	public BigDecimal getImsiCriterionNextSequence();
}
