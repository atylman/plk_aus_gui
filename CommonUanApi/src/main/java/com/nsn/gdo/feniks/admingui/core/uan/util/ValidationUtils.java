package com.nsn.gdo.feniks.admingui.core.uan.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



public class ValidationUtils {

	static Log logger = LogFactory.getLog(ValidationUtils.class);

	public static boolean validateIsEmpty(String directoryNumber) {
	
			if(directoryNumber == null || directoryNumber.equals("")){
				return true;
			}
		return false;
	}

	public static boolean isDirNumInvalid(String directoryNumber) {
		
			if(directoryNumber != null && (directoryNumber.length() > 32)){
				return true;
			}	
			return false;
	}
}


