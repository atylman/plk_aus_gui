package com.nsn.gdo.feniks.admingui.core.uan.service;

import javax.ejb.Remote;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanServiceType;


@Remote
public interface UanServiceTypeManager {

	public static final String REMOTE_JNDI_NAME  = "uan/UanServiceTypeManagerRemote";
	
	public UanServiceType getServiceType(Long serviceTypeId);
	
}
