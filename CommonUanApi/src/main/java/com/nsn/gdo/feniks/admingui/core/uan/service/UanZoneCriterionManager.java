package com.nsn.gdo.feniks.admingui.core.uan.service;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanZoneCriterion;
import com.nsn.ploc.feniks.admingui.exception.ApplicationStorageException;


@Remote
public interface UanZoneCriterionManager {

	public static final String REMOTE_JNDI_NAME = "uan/UanZoneCriterionManagerRemote";
	
	public UanZoneCriterion getZoneCriteriaForSubscrId(Long subscriptionId);
	
	public Long getZoneCriteriaIdForSubscrId(Long subscriptionId);
	
	public BigDecimal getZoneCriterionNextSequence();
	
	public UanZoneCriterion getUanZoneCriteria(long id);

	public List<UanZoneCriterion> getAllZoneCritForSubscription(
			ServiceSubscription serviceSubscription);
	
	public List<Long> getAllZoneCritIdForSubscription(Long subscriptionId);
	
	public void remove(Long critId) throws ApplicationStorageException;
}
