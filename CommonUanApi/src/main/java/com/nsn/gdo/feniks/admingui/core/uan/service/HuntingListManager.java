package com.nsn.gdo.feniks.admingui.core.uan.service;

import java.util.List;

import javax.ejb.Remote;

import com.nsn.ploc.feniks.admingui.core.commons.lists.model.HuntingList;


@Remote
public interface HuntingListManager {
	
	public static final String REMOTE_JNDI_NAME  = "commons/HuntingListManager";
	
	public HuntingList findByName(String name);	

	public List<HuntingList> findByCriteria(String criteria, int limit);

	public HuntingList findById(Long id);
	
}
