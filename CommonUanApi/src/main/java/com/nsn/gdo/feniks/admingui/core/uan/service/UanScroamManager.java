package com.nsn.gdo.feniks.admingui.core.uan.service;

import javax.ejb.Remote;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanScroamInfo;

@Remote
public interface UanScroamManager {
	
	public static final String REMOTE_JNDI_NAME  = "uan/UanScroamManagerRemote";
	
	public UanScroamInfo getScroamById(Long id);
}
