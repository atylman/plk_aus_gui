package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;


@Entity
@Table(name="UAN_IVR_CRITERION", schema=UanConstants.SCHEMA_UAN)
public class UanIvrCriterion extends CriteriaType implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private UanIvrCriterionPK id;
	
	@Column(name="ANN_ID")
	Long annId;
	
	@Column(name="TRANSLATION_TYPE")
	Long translationType;
	
	@Column(name="TRANSLATION_TYPE_ID")
	Long translationTypeId;

	@OneToOne
	@JoinColumn(name="SUBSCRIPTION_ID")
	private ServiceSubscription serviceSubscription;
	
	public UanIvrCriterionPK getId() {
		return id;
	}

	public void setId(UanIvrCriterionPK id) {
		this.id = id;
	}

	public Long getAnnId() {
		return annId;
	}

	public void setAnnId(Long annId) {
		this.annId = annId;
	}

	public Long getTranslationType() {
		return translationType;
	}

	public void setTranslationType(Long translationType) {
		this.translationType = translationType;
	}

	public Long getTranslationTypeId() {
		return translationTypeId;
	}

	public void setTranslationTypeId(Long translationTypeId) {
		this.translationTypeId = translationTypeId;
	}

	public ServiceSubscription getServiceSubscription() {
		return serviceSubscription;
	}

	public void setServiceSubscription(ServiceSubscription serviceSubscription) {
		this.serviceSubscription = serviceSubscription;
	}

	@Override
	public String toString() {
		return "UanIvrCriterion [id=" + id + ", annId=" + annId
				+ ", translationType=" + translationType
				+ ", translationTypeId=" + translationTypeId + "]";
	}

}
