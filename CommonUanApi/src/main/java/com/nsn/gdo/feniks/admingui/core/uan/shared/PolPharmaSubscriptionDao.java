package com.nsn.gdo.feniks.admingui.core.uan.shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.nsn.ploc.feniks.admingui.core.commons.lists.model.ListCommons;
import com.nsn.ploc.feniks.admingui.core.commons.lists.model.NumberList;
import com.nsn.ploc.feniks.admingui.core.commons.lists.model.NumberListSet;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanANumberCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanHuntingList;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingNumber;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;


public class PolPharmaSubscriptionDao {
	
	UanRoutingNumber uanRoutingNumber;
	UanANumberCriterion uanANumberCriterion;
	NumberListData aNumberGlobalListsSet;
	List<NumberListData> lists = new ArrayList<NumberListData>();
	String huntingListName;
	UanHuntingList uanHuntingList;
	
	protected static final String STRING_ID_SEPARATOR = "#:#";
	protected static final String NAME_ID_SEPARATOR = "@:@";
	private static final String EMPTY_JSON_ARRAY = "[]";
	

	public UanRoutingNumber getUanRoutingNumber() {
		return uanRoutingNumber;
	}
	public void setUanRoutingNumber(UanRoutingNumber uanRoutingNumber) {
		this.uanRoutingNumber = uanRoutingNumber;
	}
	public UanANumberCriterion getUanANumberCriterion() {
		return uanANumberCriterion;
	}
	public void setUanANumberCriterion(UanANumberCriterion uanANumberCriterion) {
		this.uanANumberCriterion = uanANumberCriterion;
	}
	public NumberListData getaNumberGlobalListsSet() {
		return aNumberGlobalListsSet;
	}
	public void setaNumberGlobalListsSet(NumberListData aNumberGlobalListsSet) {
		this.aNumberGlobalListsSet = aNumberGlobalListsSet;
	}
	public String getHuntingListName() {
		return huntingListName;
	}
	public void setHuntingListName(String huntingListName) {
		this.huntingListName = huntingListName;
	}
	public UanHuntingList getUanHuntingList() {
		return uanHuntingList;
	}
	public void setUanHuntingList(UanHuntingList uanHuntingList) {
		this.uanHuntingList = uanHuntingList;
	}
	public List<NumberListData> getLists() {
		return lists;
	}
	public void setLists(List<NumberListData> lists) {
		this.lists = lists;
	}
	
	public String getListsAsJSON() {
		String returnString = EMPTY_JSON_ARRAY;
		if(uanANumberCriterion.getMatchingOption().intValue() == 
			UanConstants.UanANumberCriteriaMatchingOption.NUMBER_LIST.getValue()){
			returnString = new Gson().toJson(lists);
		}
		
		return returnString;
	}
	
	public void setLists(String stringForm) {
		lists = new ArrayList<NumberListData>();
		String[] idsArray = stringForm.split(STRING_ID_SEPARATOR);
		
		for (String id : idsArray) {
			String[] pair = id.split(NAME_ID_SEPARATOR);
		
			if(pair.length == 2) {
		
				lists.add(new NumberListData(new Long(pair[0]), pair[1]));
			}
		}
	}	
	
	public String getListsNamesAsString() {
		StringBuffer sb = new StringBuffer("[");
		List<String> tmp = new ArrayList<String>();
		for (NumberListData list : lists) {
			tmp.add(list.name);
		}
		if(tmp.size() > 0) {
			Collections.sort(tmp);
			sb.append(StringUtils.join(tmp, ", "));
		} else {
			sb.append(" ");
		}
		sb.append("]");
		return sb.toString();
	}	
	
	public NumberListSet getUpdatedSet() {
		if(uanANumberCriterion.getMatchingOption().intValue() == UanConstants.UanANumberCriteriaMatchingOption.NUMBER_LIST.getValue()){
			NumberListSet set = new NumberListSet();
			if(getLists() != null && getLists().size() != 0) {	
				set.setSetId(null);
				set.setLists(new HashSet<NumberList>());
				List<Long> result = new ArrayList<Long>();
				for (NumberListData list : getLists()) {
						//result.add(list.getKey());
						result.add(list.getId());
					}
				for(Long listId : result) {
					NumberList tsList = new NumberList();
					tsList.setId(listId);
					set.getLists().add(tsList);
				}
			}
			return set;
		} else if (uanANumberCriterion.getMatchingOption().intValue() == UanConstants.UanANumberCriteriaMatchingOption.ANY.getValue()){
			return getAnySet();
		} else {
			return null;
		}
	}	
	
	public void setListData(NumberListSet set) {
		if(set != null && set.getLists() != null) {
			for (ListCommons list : set.getLists()) {
				lists.add(new NumberListData(list.getId(), list.getName()));				
			}
		}
	}
	
	public boolean isSelectionValid() {
		if(lists.isEmpty()) {
			return false;
		}
		for (NumberListData list : lists) {	
			//if(list.getKey() == null) {
			if(list.getId() == null) {
				return false;
			}
		}
		return true;
	}
	
	private NumberListSet getAnySet() {
		NumberListSet set = new NumberListSet();
		set.setSetId(1L);
		NumberList list = new NumberList();
		list.setId(1L);
		list.setName("ANY");
		set.setLists(Collections.singleton(list));
		return set;
	}
	
	@Override
	public String toString() {
		return "PolPharmaSubscriptionDao [uanRoutingNumber=" + uanRoutingNumber
				+ ", uanANumberCriterion=" + uanANumberCriterion
				+ ", aNumberGlobalListsSet=" + aNumberGlobalListsSet
				+ ", lists=" + lists + ", huntingListName=" + huntingListName
				+ ", uanHuntingList=" + uanHuntingList + "]";
	}
	

}
