package com.nsn.gdo.feniks.admingui.core.uan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

@Entity
@Table(name = "UAN_CONFIG_PARAM", schema = UanConstants.SCHEMA_UAN)
@IdClass(UanConfigParamPK.class)
public class UanConfigParam {

	private static final long serialVersionUID = 1L;

	@Id
	String paramName;
	
	@Id
	Long serviceType;

	@Column(name = "PARAM_VALUE")
	String paramValue;

	@Column(name = "PARAM_DESC")
	String paramDesc;
	
	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getParamDesc() {
		return paramDesc;
	}

	public void setParamDesc(String paramDesc) {
		this.paramDesc = paramDesc;
	}

	public Long getServiceType() {
		return serviceType;
	}

	public void setServiceType(Long serviceType) {
		this.serviceType = serviceType;
	}
	
	@Override
	public String toString() {
		return "UanConfigParam [paramName=" + paramName + ", paramValue=" +paramValue
		+ ", paramDesc=" + paramDesc + ", serviceType=" + serviceType
		+ "]";	}
}
