package com.nsn.gdo.feniks.admingui.core.uan.service;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingConfiguration;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingNumber;


@Remote
public interface UanRoutingNumberManager {

	public static final String REMOTE_JNDI_NAME  = "uan/UanRoutingNumberManagerRemote";
	
	public UanRoutingNumber getRoutingNumberById(Long rnId);
	
	public BigDecimal getRoutingNumberNextSequence();

	public List<UanRoutingNumber> getAllForSubscription(Long subscriptionId);
	
	public List<Long> getAllRoutingNumberIdForChainedRouteConfig(UanRoutingConfiguration firstChainedRouteConfigObj);
}
