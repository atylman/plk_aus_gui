package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

@Entity
@Table(name="UAN_SCROAM_INFO", schema=UanConstants.SCHEMA_UAN)
public class UanScroamInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
    @SequenceGenerator(name="SCROAM_INFO_ID_GENERATOR", sequenceName="SCROAM_INFO_ID_SQ", allocationSize=1, initialValue=1)
	@GeneratedValue(generator="SCROAM_INFO_ID_GENERATOR")
	@Column(name="SCROAM_INFO_ID", unique=true, nullable=false)
	Long scroamInfoId;

	@Column(name="SCROAM_OPERATOR")
	String scroamOperator;
	
	@Column(name="SCROAM_COUNTRY")
	String scroamCountry;
	
	@Column(name="SCROAM_SERVICE_NAME")
	String scroamServiceName;
	
	
	public Long getScroamInfoId() {
		return scroamInfoId;
	}

	public void setScroamInfoId(Long scroamInfoId) {
		this.scroamInfoId = scroamInfoId;
	}

	public String getScroamOperator() {
		return scroamOperator;
	}

	public void setScroamOperator(String scroamOperator) {
		this.scroamOperator = scroamOperator;
	}

	public String getScroamCountry() {
		return scroamCountry;
	}

	public void setScroamCountry(String scroamCountry) {
		this.scroamCountry = scroamCountry;
	}

	public String getScroamServiceName() {
		return scroamServiceName;
	}

	public void setScroamServiceName(String scroamServiceName) {
		this.scroamServiceName = scroamServiceName;
	}

	@Override
	public String toString() {
		return "UanScroamInfo [scroamInfoId=" + scroamInfoId
				+ ", scroamOperator=" + scroamOperator + ", scroamCountry="
				+ scroamCountry + ", scroamServiceName=" + scroamServiceName
				+ "]";
	}	
}
