package com.nsn.gdo.feniks.admingui.core.uan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

@Entity
@Table(name = "UAN_HUNTING_LIST", schema = UanConstants.SCHEMA_UAN)
public class UanHuntingList {
	
	@Id
	@Column(name = "HL_ID")
	@SequenceGenerator(name="HUNTING_ID_GENERATOR", sequenceName="HUNTING_ID_SQ", allocationSize=1, initialValue=1)
	@GeneratedValue(generator="HUNTING_ID_GENERATOR")
	private Long id;
	
	@Column(name = "Number_1")
	private String hlNumber1;
	
	@Column(name = "Number_2")
	private String hlNumber2;
	
	@Column(name = "Number_3")
	private String hlNumber3;
	
	@Column(name = "Number_4")
	private String hlNumber4;
	
	@Column(name = "Number_5")
	private String hlNumber5;
	
	@Column(name = "TON")
	private Long ton;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHlNumber1() {
		return hlNumber1;
	}

	public void setHlNumber1(String hlNumber1) {
		this.hlNumber1 = hlNumber1;
	}

	public String getHlNumber2() {
		return hlNumber2;
	}

	public void setHlNumber2(String hlNumber2) {
		this.hlNumber2 = hlNumber2;
	}

	public String getHlNumber3() {
		return hlNumber3;
	}

	public void setHlNumber3(String hlNumber3) {
		this.hlNumber3 = hlNumber3;
	}

	public String getHlNumber4() {
		return hlNumber4;
	}

	public void setHlNumber4(String hlNumber4) {
		this.hlNumber4 = hlNumber4;
	}

	public String getHlNumber5() {
		return hlNumber5;
	}

	public void setHlNumber5(String hlNumber5) {
		this.hlNumber5 = hlNumber5;
	}

	public Long getTon() {
		return ton;
	}

	public void setTon(Long ton) {
		this.ton = ton;
	}

	@Override
	public String toString() {
		return "UanHuntingList [id=" + id + ", hlNumber1=" + hlNumber1
				+ ", hlNumber2=" + hlNumber2 + ", hlNumber3=" + hlNumber3
				+ ", hlNumber4=" + hlNumber4 + ", hlNumber5=" + hlNumber5
				+ ", ton=" + ton + "]";
	}


	@PrePersist 
    @PreUpdate
    private void toUpperCase() {
       
          if(getHlNumber1() != null)
          {
        	  hlNumber1 = getHlNumber1().toUpperCase();
          }
          if(getHlNumber2() != null)
          {
        	  hlNumber2 = getHlNumber2().toUpperCase();
          }
          if(getHlNumber3() != null)
          {
        	  hlNumber3 = getHlNumber3().toUpperCase();
          }
       
          if(getHlNumber4() != null)
          {
        	  hlNumber4 = getHlNumber4().toUpperCase();
          }
       
          if(getHlNumber5() != null)
          {
        	  hlNumber5 = getHlNumber5().toUpperCase();
          }
       
    }
	
}
