package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UanBearerCriterionPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="BEARER_CRIT_ID", nullable=false)	
	private long bearerCritId;

	@Column(name="BEARER", nullable=false)
	private long bearer;
	
    public UanBearerCriterionPK() {
    }
	public long getBearerCritId() {
		return this.bearerCritId;
	}
	public void setBearerCritId(long bearerCritId) {
		this.bearerCritId = bearerCritId;
	}
	

	public long getBearer() {
		return bearer;
	}
	public void setBearer(long bearer) {
		this.bearer = bearer;
	}
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UanBearerCriterionPK)) {
			return false;
		}
		UanBearerCriterionPK castOther = (UanBearerCriterionPK)other;
		return 
			(this.bearerCritId == castOther.bearerCritId)
			&& (this.bearer == castOther.bearer);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.bearerCritId ^ (this.bearerCritId >>> 32)));
		hash = hash * prime + ((int) (this.bearer ^ (this.bearer >>> 32)));
		
		return hash;
    }
	
	@Override
	public String toString() {
		return "UanBearerCriterionPK [bearerCritId=" + bearerCritId + ", bearer="
				+ bearer + "]";
	}

}
