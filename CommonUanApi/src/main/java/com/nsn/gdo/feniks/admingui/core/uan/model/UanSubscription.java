 package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.nsn.ploc.feniks.admingui.core.commons.announ.model.Announcement;
import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

@Entity
@Table(name="UAN_SUBSCRIPTION", schema=UanConstants.SCHEMA_UAN)
public class UanSubscription implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SUBSCRIPTION_ID")
	Long subscriptionID;
	
	@OneToOne(cascade={CascadeType.MERGE})
	@PrimaryKeyJoinColumn
	private ServiceSubscription serviceSubs;	
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="SCREENING_PROFILE_ID")
	UanScreening screeningProfileID;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="WELCOME_ANN_ID")
	Announcement welcomeAnnID;
	
	@Column(name="ANN_INT_FLAG")
	Long annIntFlag;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="RR_ID")	
	UanRoutingConfiguration rrID;
	
	@Column(name="SCI_ID")
	Long sciID;

	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="FCI_ID")	
	UanFci fciId;
	
	@Column(name="DEFAULT_TREATMENT_NUMBER")
	String defaultTreatmentNumber;
	
	@Column(name="DEFAULT_TREATMENT_NUMBER_VIDEO")
	String defaultTreatmentNumberVideo;
	
	@Column(name="TON")
	Long ton;
	
	@Column(name="DEFAULT_TREATMENT_NUMBER_HL")
	String defaultTreatmentNumberList;
	
	@Column(name="TON_HL")
	Long ton_list; 
	
	@Column(name="DEFAULT_TREATMENT_NUMBER_ERROR")
	String defaultTreatmentNumberError;
	
	@Column(name="TON_ERROR")
	Long tonError;
	
	@Column(name="DEFAULT_TREATMENT_NUMBER_CONF")
	String defaultTreatmentNumberSubs;
	
	@Column(name="TON_CONF")
	Long tonConf;
	
	@Column(name="TON_VIDEO")
	Long tonVideo;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="FCI_ID_NO_MATCH")	
	UanFci fciIdForNoMatch;

	@Column(name="ANONIMIZATION_FLAG" , columnDefinition="Number(1,0) default 0")
	Boolean anonimizationFlag=false;
	
	@Column(name="CALL_COMPLETION_ACTION")
	Long callCompletionAction;
	
	public Long getSubscriptionID() {
		return subscriptionID;
	}

	public void setSubscriptionID(Long subscriptionID) {
		this.subscriptionID = subscriptionID;
	}

	public ServiceSubscription getServiceSubs() {
		return serviceSubs;
	}

	public void setServiceSubs(ServiceSubscription serviceSubs) {
		this.serviceSubs = serviceSubs;
	}

	public UanScreening getScreeningProfileID() {
		return screeningProfileID;
	}

	public void setScreeningProfileID(UanScreening screeningProfileID) {
		this.screeningProfileID = screeningProfileID;
	}

	public Announcement getWelcomeAnnID() {
		return welcomeAnnID;
	}

	public void setWelcomeAnnID(Announcement welcomeAnnID) {
		this.welcomeAnnID = welcomeAnnID;
	}

	public Long getAnnIntFlag() {
		return annIntFlag;
	}

	public void setAnnIntFlag(Long annIntFlag) {
		this.annIntFlag = annIntFlag;
	}

	public UanRoutingConfiguration getRrID() {
		return rrID;
	}

	public void setRrID(UanRoutingConfiguration rrID) {
		this.rrID = rrID;
	}

	public Long getSciID() {
		return sciID;
	}

	public void setSciID(Long sciID) {
		this.sciID = sciID;
	}

	public UanFci getFciId() {
		return fciId;
	}

	public void setFciId(UanFci fciId) {
		this.fciId = fciId;
	}

	public String getDefaultTreatmentNumber() {
		return defaultTreatmentNumber;
	}

	public void setDefaultTreatmentNumber(String defaultTreatmentNumber) {
		this.defaultTreatmentNumber = defaultTreatmentNumber;
	}

	public String getDefaultTreatmentNumberVideo() {
		return defaultTreatmentNumberVideo;
	}

	public void setDefaultTreatmentNumberVideo(String defaultTreatmentNumberVideo) {
		this.defaultTreatmentNumberVideo = defaultTreatmentNumberVideo;
	}

	public Long getTon() {
		return ton;
	}

	public void setTon(Long ton) {
		this.ton = ton;
	}

	public String getDefaultTreatmentNumberList() {
		return defaultTreatmentNumberList;
	}

	public void setDefaultTreatmentNumberList(String defaultTreatmentNumberList) {
		this.defaultTreatmentNumberList = defaultTreatmentNumberList;
	}

	public Long getTon_list() {
		return ton_list;
	}

	public void setTon_list(Long ton_list) {
		this.ton_list = ton_list;
	}

	public String getDefaultTreatmentNumberError() {
		return defaultTreatmentNumberError;
	}

	public void setDefaultTreatmentNumberError(String defaultTreatmentNumberError) {
		this.defaultTreatmentNumberError = defaultTreatmentNumberError;
	}

	public Long getTonError() {
		return tonError;
	}

	public void setTonError(Long tonError) {
		this.tonError = tonError;
	}

	public String getDefaultTreatmentNumberSubs() {
		return defaultTreatmentNumberSubs;
	}

	public void setDefaultTreatmentNumberSubs(String defaultTreatmentNumberSubs) {
		this.defaultTreatmentNumberSubs = defaultTreatmentNumberSubs;
	}

	public Long getTonConf() {
		return tonConf;
	}

	public void setTonConf(Long tonConf) {
		this.tonConf = tonConf;
	}

	public Long getTonVideo() {
		return tonVideo;
	}

	public void setTonVideo(Long tonVideo) {
		this.tonVideo = tonVideo;
	}
	
	public Boolean getAnonimizationFlag() {
		return anonimizationFlag;
	}

	public void setAnonimizationFlag(Boolean anonimizationFlag) {
		this.anonimizationFlag = anonimizationFlag;
	}

	public Long getCallCompletionAction() {
		return callCompletionAction;
	}

	public void setCallCompletionAction(Long callCompletionAction) {
		this.callCompletionAction = callCompletionAction;
	}	

	@Override
	public String toString() {
		return "UanSubscription [subscriptionID=" + subscriptionID
				+ ", serviceSubs=" + serviceSubs + ", screeningProfileID="
				+ screeningProfileID + ", welcomeAnnID=" + welcomeAnnID
				+ ", annIntFlag=" + annIntFlag + ", rrID=" + rrID + ", sciID="
				+ sciID + ", fciId=" + fciId + ", defaultTreatmentNumber="
				+ defaultTreatmentNumber + ", defaultTreatmentNumberVideo="
				+ defaultTreatmentNumberVideo + ", ton=" + ton
				+ ", defaultTreatmentNumberList=" + defaultTreatmentNumberList
				+ ", ton_list=" + ton_list + ", defaultTreatmentNumberError="
				+ defaultTreatmentNumberError + ", tonError=" + tonError
				+ ", defaultTreatmentNumberSubs=" + defaultTreatmentNumberSubs
				+ ", tonConf=" + tonConf + ", tonVideo=" + tonVideo 
				+ ", fciIdForNoMatch=" + fciIdForNoMatch 
				+ ", anonimizationFlag=" + anonimizationFlag 
				+ ", callCompletionAction=" + callCompletionAction +"]";
	}

	public UanFci getFciIdForNoMatch() {
		return fciIdForNoMatch;
	}

	public void setFciIdForNoMatch(UanFci fciIdForNoMatch) {
		this.fciIdForNoMatch = fciIdForNoMatch;
	}
	
	@PrePersist 
    @PreUpdate
    private void toUpperCase() {
          
          if(getDefaultTreatmentNumber() != null)
          {
        	  defaultTreatmentNumber = getDefaultTreatmentNumber().toUpperCase();
          }
          
          if(getDefaultTreatmentNumberList() != null)
          {
        	  defaultTreatmentNumberList = getDefaultTreatmentNumberList().toUpperCase();
          }
          
          if(getDefaultTreatmentNumberError() != null)
          {
        	  defaultTreatmentNumberError = getDefaultTreatmentNumberError().toUpperCase();
          }
          
          if(getDefaultTreatmentNumberVideo() != null)
          {
        	  defaultTreatmentNumberVideo = getDefaultTreatmentNumberVideo().toUpperCase();
          }
          
          if(getDefaultTreatmentNumberSubs() != null)
          {
        	  defaultTreatmentNumberSubs = getDefaultTreatmentNumberSubs().toUpperCase();
          }
          
    }
}
