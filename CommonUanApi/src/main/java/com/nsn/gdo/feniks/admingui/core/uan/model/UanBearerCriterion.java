package com.nsn.gdo.feniks.admingui.core.uan.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;



@Entity
@Table(name="UAN_BEARER_CRITERION", schema = UanConstants.SCHEMA_UAN)
public class UanBearerCriterion extends CriteriaType implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private UanBearerCriterionPK id;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="RN_ID")
	UanRoutingNumber rnId;

	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="SUBSCRIPTION_ID")
	private ServiceSubscription subscriptionId;

	public UanBearerCriterionPK getId() {
		return id;
	}

	public void setId(UanBearerCriterionPK id) {
		this.id = id;
	}

	public UanRoutingNumber getRnId() {
		return rnId;
	}

	public void setRnId(UanRoutingNumber rnId) {
		this.rnId = rnId;
	}

	public ServiceSubscription getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(ServiceSubscription subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	
	@Override
	public String toString() {
		return "UanBearerCriterion [id=" + id + ", rnId=" + rnId +  ", subscriptionId=" + subscriptionId + "]";
	}

	

	
}