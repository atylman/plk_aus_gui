package com.nsn.gdo.feniks.admingui.core.uan.service;

import javax.ejb.Remote;

import java.util.List;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanFci;


@Remote
public interface UanFciManager {

	public static final String REMOTE_JNDI_NAME  = "uan/UanFciManagerRemote";
	
	public UanFci getFCIById(Long id);
	
	public List<UanFci> getAll();
	
}
