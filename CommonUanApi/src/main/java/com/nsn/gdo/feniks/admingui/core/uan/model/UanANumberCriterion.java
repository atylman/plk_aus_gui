package com.nsn.gdo.feniks.admingui.core.uan.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;

import com.nsn.ploc.feniks.admingui.core.commons.lists.model.NumberListSet;
import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

@Entity
@Table(name = "UAN_ANUMBER_CRITERION", schema = UanConstants.SCHEMA_UAN)
public class UanANumberCriterion {
	
	@Id
	@Column(name = "ANUM_CRIT_ID")
	private Long id;
	
	@Column(name = "NUM_TO_MATCH")
	private Long numToMatch;
	
	@Column(name = "MATCHING_OPTION")
	private Long matchingOption;
	
	@Column(name = "DIRNUM_NUMPREFIX")
	private String dirNumber;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "NUM_GLOBAL_LIST_ID")
	private NumberListSet globalListSet;
	
	@OneToOne
	@JoinColumn(name="SUBSCRIPTION_ID")
	private ServiceSubscription serviceSubscription;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumToMatch() {
		return numToMatch;
	}

	public void setNumToMatch(Long numToMatch) {
		this.numToMatch = numToMatch;
	}

	public Long getMatchingOption() {
		return matchingOption;
	}

	public void setMatchingOption(Long matchingOption) {
		this.matchingOption = matchingOption;
	}

	public String getDirNumber() {
		return dirNumber;
	}

	public void setDirNumber(String dirNumber) {
		this.dirNumber = dirNumber;
	}

	public NumberListSet getGlobalListSet() {
		return globalListSet;
	}

	public void setGlobalListSet(NumberListSet globalListSet) {
		this.globalListSet = globalListSet;
	}

	public boolean isCustomNumberListSet() {
		return (globalListSet != null && globalListSet.isCustomSet()) ? true : false;
	}
	
	public boolean isAnyNumberListSet() {
		return (globalListSet != null && globalListSet.getSetId().intValue() == UanConstants.AnyListSetValue.intValue()) ? true : false;
	}
	
	public ServiceSubscription getServiceSubscription() {
		return serviceSubscription;
	}

	public void setServiceSubscription(ServiceSubscription serviceSubscription) {
		this.serviceSubscription = serviceSubscription;
	}

	
	@Override
	public String toString() {
		
		return "UanANumberCriterion [id=" + id + ", numToMatch=" + numToMatch
		+ ", matchingOption=" + matchingOption + ", dirNumber=" + dirNumber +  ", globalListSet=" + globalListSet + "]";
	}
	
	 	@PrePersist 
	    @PreUpdate
	    private void toUpperCase() {
	     
	 		if(getDirNumber() != null)
	          {
	        	  dirNumber = getDirNumber().toUpperCase();
	          }
	     }
	
}
