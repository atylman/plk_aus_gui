package com.nsn.gdo.feniks.admingui.core.uan.service;

import java.util.List;

import javax.ejb.Remote;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanConfigParam;

@Remote
public interface UanConfigParamManager {
	
	public static final String REMOTE_JNDI_NAME  = "uan/UanConfigParamManager";
	
	public List<UanConfigParam > findByServiceId(Long serviceId);
	
	public List<UanConfigParam > getAll();

}
