package com.nsn.gdo.feniks.uan.commons.core.manager;


import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanScreening;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanScreeningManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;


@Stateless(name="UanScreeningManagerBean")
@RemoteBinding(jndiBinding=UanScreeningManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanScreeningManagerBean extends BaseManager<UanScreening, Long> implements UanScreeningManager {

	Log logger = LogFactory.getLog(UanScreeningManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	
	@Override
	public UanScreening getScreeningByProfileId(Long screeningProfileId) {
		return super.get(screeningProfileId); 
	}
	
	@Override
	protected Class<UanScreening> getEntityClass() {
		return UanScreening.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
		
}
