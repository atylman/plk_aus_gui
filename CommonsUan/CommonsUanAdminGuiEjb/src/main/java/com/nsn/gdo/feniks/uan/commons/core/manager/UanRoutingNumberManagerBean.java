package com.nsn.gdo.feniks.uan.commons.core.manager;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingConfiguration;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingNumber;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanRoutingNumberManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants.UanCriterionCriteriaTypes;


@Stateless(name="UanRoutingNumberManagerBean")
@RemoteBinding(jndiBinding=UanRoutingNumberManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanRoutingNumberManagerBean extends BaseManager<UanRoutingNumber, Long> implements UanRoutingNumberManager {

	Log logger = LogFactory.getLog(UanRoutingNumberManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	
	@Override
	public UanRoutingNumber getRoutingNumberById(Long rnId) {
		return super.get(rnId); 
	}
	
	@Override
	protected Class<UanRoutingNumber> getEntityClass() {
		return UanRoutingNumber.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
		
	
	public BigDecimal getRoutingNumberNextSequence() {
		Query namedQuery1 = getEntityManager().createNativeQuery("Select UAN.RN_ID_SQ.NEXTVAL from Dual");
		
		@SuppressWarnings("unused")
		BigDecimal nextSequence1 = ((BigDecimal)namedQuery1.getResultList().get(0));		
		Query namedQuery = getEntityManager().createNativeQuery("Select UAN.RN_ID_SQ.CURRVAL from Dual");
		return ((BigDecimal)namedQuery.getResultList().get(0));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UanRoutingNumber> getAllForSubscription(Long subscriptionId)
	{
		Query query = getEntityManager().createQuery("select routing from UanRoutingNumber routing where routing.serviceSubscription.subscriptionId= ?");
		query.setParameter(1, subscriptionId);
		List<UanRoutingNumber> list = query.getResultList();
		return list;
	}

	
	public List<Long> getAllRoutingNumberIdForChainedRouteConfig(UanRoutingConfiguration firstChainedRouteConfigObj) {
		List<Long> routingNumbIdList = new ArrayList<Long>();
		UanRoutingConfiguration oldRoutingConfiguration = null;
		UanRoutingConfiguration newRoutingConfiguration = null;
		
		newRoutingConfiguration = firstChainedRouteConfigObj;
		
		if(newRoutingConfiguration != null){
			while(newRoutingConfiguration.getCrrIDForNomatch() != null) {
				oldRoutingConfiguration = newRoutingConfiguration;
				newRoutingConfiguration = oldRoutingConfiguration.getCrrIDForNomatch();
				if(oldRoutingConfiguration.getCritID().getCritType().intValue() == UanCriterionCriteriaTypes.A_NUMBER.getValue()){
					routingNumbIdList.add(oldRoutingConfiguration.getCrnForMatch().getRnID());
				}				
			}
			if(newRoutingConfiguration.getCrrIDForNomatch() == null){
				routingNumbIdList.add(newRoutingConfiguration.getCrnForMatch().getRnID());
			}
		}
		return routingNumbIdList;
	}	
	
}
