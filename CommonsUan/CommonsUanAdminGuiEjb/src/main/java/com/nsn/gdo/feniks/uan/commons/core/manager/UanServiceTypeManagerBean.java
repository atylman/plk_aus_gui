package com.nsn.gdo.feniks.uan.commons.core.manager;


import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanServiceType;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanServiceTypeManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;


@Stateless(name="UanServiceTypeManagerBean")
@RemoteBinding(jndiBinding=UanServiceTypeManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanServiceTypeManagerBean extends BaseManager<UanServiceType, Long> implements UanServiceTypeManager {

	Log logger = LogFactory.getLog(UanServiceTypeManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	
	@Override
	public UanServiceType getServiceType(Long serviceTypeId) {
		return super.get(serviceTypeId); 
	}
	
	@Override
	protected Class<UanServiceType> getEntityClass() {
		return UanServiceType.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
		
}
