package com.nsn.gdo.feniks.uan.commons.core.manager;


import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.gdo.feniks.admingui.core.uan.model.UanBearerCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingConfiguration;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanBearerCriterionManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanSubscriptionManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants.UanCriterionCriteriaTypes;
import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;

@Stateless(name = "UanBearerCriterionManagerBean")
@RemoteBinding(jndiBinding = UanBearerCriterionManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanBearerCriterionManagerBean extends
		BaseManager<UanBearerCriterion, Long> implements
		UanBearerCriterionManager {

	Log logger = LogFactory.getLog(UanBearerCriterionManagerBean.class);

	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;

	@EJB
	UanSubscriptionManager subscriptionManager;
	
	@Override
	protected Class<UanBearerCriterion> getEntityClass() {
		return UanBearerCriterion.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List getBearerCriteriaForSubscrId(Long subscriptionId) {
		List uanBearerCriterionList = null;
		Long criteriaTypeId;

		// get data from uan_subscription table(screening profile id)
		UanSubscription uanSubscription = subscriptionManager
				.getSubscriptionById(subscriptionId);
		UanRoutingConfiguration uanRoutingConfiguration = uanSubscription
				.getRrID();

		UanCriterion uanCriterion = uanRoutingConfiguration.getCritID();
		if (uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.BEARER
				.getValue()) {
			criteriaTypeId = uanCriterion.getCritTypeId();
			uanBearerCriterionList = getBearerCriterionList(criteriaTypeId);
		}
		return uanBearerCriterionList;
	}

	@Override
	public Long getBearerCriteriaIdForSubscrId(Long subscriptionId) {
		Long criteriaTypeId = null;

		// get data from uan_subscription table(screening profile id)
		UanSubscription uanSubscription = subscriptionManager
				.getSubscriptionById(subscriptionId);
		UanRoutingConfiguration uanRoutingConfiguration = uanSubscription
				.getRrID();

		UanCriterion uanCriterion = uanRoutingConfiguration.getCritID();

		if (uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.BEARER
				.getValue()) {
			criteriaTypeId = uanCriterion.getCritTypeId();
		}
		return criteriaTypeId;
	}

	@Override
	public BigDecimal getBearerCriterionNextSequence() {
		Query namedQuery1 = getEntityManager().createNativeQuery(
				"Select UAN.CRITERION_TYPE_ID_SQ.NEXTVAL from Dual");
		@SuppressWarnings("unused")
		BigDecimal nextSequence1 = ((BigDecimal) namedQuery1.getResultList()
				.get(0));
		Query namedQuery = getEntityManager().createNativeQuery(
				"Select UAN.CRITERION_TYPE_ID_SQ.CURRVAL from Dual");
		return ((BigDecimal) namedQuery.getResultList().get(0));
	}

	@SuppressWarnings("rawtypes")
	private List getBearerCriterionList(Long critId) {
		Query query = getEntityManager().createQuery(
				"select o from " + getEntityClass().getSimpleName()
						+ " o where o.id.bearerCritId = ?");
		query.setParameter(1, critId);
		return query.getResultList();
	}
	
	@SuppressWarnings("rawtypes")
	public List<UanBearerCriterion> getUanBearerCriterionList(Long critId) {
		Query query = getEntityManager().createQuery("select o from " + getEntityClass().getSimpleName()
						+ " o where o.id.bearerCritId = ?");
		query.setParameter(1, critId);
		return query.getResultList();
	}

}

