package com.nsn.gdo.feniks.uan.commons.core.manager;

import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.commons.lists.model.HuntingList;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.service.HuntingListManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

/**
 * Session Bean for UAN Configuration parameters management
 *
 */
@Stateless(name="HuntingListManagerBean")
@RemoteBinding(jndiBinding=HuntingListManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class HuntingListManagerBean extends BaseManager<HuntingList, Long> implements HuntingListManager {

	@PersistenceContext(unitName=UanConstants.PERSISTENCE_UNIT_NAME)
	protected EntityManager em;		
	
	
	@Override
	protected EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return em;
	}

	@Override
	protected Class<HuntingList> getEntityClass() {
		return HuntingList.class;
	}

	@Override
	public HuntingList findByName(String name) {
		Query query = getEntityManager().createQuery("select o from " + getEntityClass().getSimpleName() + " o where o.name = ?");
		query.setParameter(1, name);
		return (HuntingList) getFirstResult(query); 
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<HuntingList> findByCriteria(String criteria, int limit) {
		Query query = getEntityManager().createQuery("select o from " + getEntityClass().getSimpleName() + " o where upper(o.name) like upper(?)");
		query.setParameter(1, replaceWildcards(criteria));
		query.setMaxResults(limit);
		return query.getResultList();
	}

		
	@Override
	public HuntingList findById(Long id) {
	
		return (HuntingList) super.find(id); 
	}
	
}
