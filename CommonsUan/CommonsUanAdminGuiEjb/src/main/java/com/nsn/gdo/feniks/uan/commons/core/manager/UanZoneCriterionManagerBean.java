package com.nsn.gdo.feniks.uan.commons.core.manager;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.ploc.feniks.admingui.core.profiles.model.ServiceSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingConfiguration;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanScreening;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanZoneCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanCriterionManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanRoutingConfigurationManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanScreeningManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanSubscriptionManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanZoneCriterionManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants.UanCriterionCriteriaTypes;
import com.nsn.ploc.feniks.admingui.exception.ApplicationStorageException;


@Stateless(name="UanZoneCriterionManagerBean")
@RemoteBinding(jndiBinding=UanZoneCriterionManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanZoneCriterionManagerBean extends BaseManager<UanZoneCriterion, Long> implements UanZoneCriterionManager {

	Log logger = LogFactory.getLog(UanZoneCriterionManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	@EJB
	UanSubscriptionManager subscriptionManager;
	
	@EJB
	UanScreeningManager screeningManager;
	
	@EJB
	UanCriterionManager criterionManager;
	
	@EJB
	UanRoutingConfigurationManager uanRoutingConfigManager;
	
	@Override
	public UanZoneCriterion getZoneCriteriaForSubscrId(Long subscriptionId) {
		UanZoneCriterion uanZoneCriterion = null;
		Long criteriaTypeId;

		UanSubscription uanSubscription = subscriptionManager.getSubscriptionById(subscriptionId);
		UanScreening uanScreening = uanSubscription.getScreeningProfileID();
		UanCriterion uanCriterion = uanScreening.getCritId();
		
		if(uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.ZONE.getValue()){
			criteriaTypeId = uanCriterion.getCritTypeId();
			uanZoneCriterion = getEntityManager().find(UanZoneCriterion.class, criteriaTypeId); 
		}
		
		return uanZoneCriterion;
	}
	
	
	@Override
	public Long getZoneCriteriaIdForSubscrId(Long subscriptionId) {
		Long criteriaTypeId = null;

		UanSubscription uanSubscription = subscriptionManager.getSubscriptionById(subscriptionId);
		UanScreening uanScreening = uanSubscription.getScreeningProfileID();
		UanCriterion uanCriterion = uanScreening.getCritId();
	
		if(uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.ZONE.getValue()){
			criteriaTypeId = uanCriterion.getCritTypeId();
		}
		
		return criteriaTypeId;
	}
	
	
	public BigDecimal getZoneCriterionNextSequence(){
		Query namedQuery1 = getEntityManager().createNativeQuery("Select UAN.CRITERION_TYPE_ID_SQ.NEXTVAL from Dual");
		@SuppressWarnings("unused")
		BigDecimal nextSequence1 = ((BigDecimal)namedQuery1.getResultList().get(0));		
		Query namedQuery = getEntityManager().createNativeQuery("Select UAN.CRITERION_TYPE_ID_SQ.CURRVAL from Dual");
		return ((BigDecimal)namedQuery.getResultList().get(0));
	}
	
	@Override
	protected Class<UanZoneCriterion> getEntityClass() {
		return UanZoneCriterion.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	public UanZoneCriterion getUanZoneCriteria(long id){
		return super.get(id);
	}
		
	@SuppressWarnings("unchecked")
	@Override
	public List<UanZoneCriterion> getAllZoneCritForSubscription(ServiceSubscription serviceSubscription)
	{
		Query query = getEntityManager().createQuery("select crit from UanZoneCriterion crit where crit.serviceSubscription= ?1");
		query.setParameter(1, serviceSubscription);
		List<UanZoneCriterion> list = query.getResultList();
		return list;
	}
	
	
	public List<Long> getAllZoneCritIdForSubscription(Long subscriptionId) {
		List<Long> zoneCritIdList = new ArrayList<Long>();
		UanRoutingConfiguration oldRoutingConfiguration = null;
		UanRoutingConfiguration newRoutingConfiguration = null;
		
		UanRoutingConfiguration uanRoutConfig = uanRoutingConfigManager.getRoutingConfigBySubsId(subscriptionId);

		if(uanRoutConfig.getCritID().getCritType().intValue() == UanCriterionCriteriaTypes.ZONE.getValue()) {
			zoneCritIdList.add(uanRoutConfig.getCritID().getCritTypeId());
		}
		
		newRoutingConfiguration = uanRoutConfig;
		
		if(newRoutingConfiguration != null){
			while(newRoutingConfiguration.getCrrIDForNomatch() != null) {
				oldRoutingConfiguration = newRoutingConfiguration;
				newRoutingConfiguration = oldRoutingConfiguration.getCrrIDForNomatch();
				if(oldRoutingConfiguration.getCritID().getCritType().intValue() == UanCriterionCriteriaTypes.ZONE.getValue()){
					zoneCritIdList.add(oldRoutingConfiguration.getCritID().getCritTypeId());
				}				
			}
			if(newRoutingConfiguration.getCrrIDForNomatch() == null){
				zoneCritIdList.add(newRoutingConfiguration.getCritID().getCritTypeId());
			}
		}
		return zoneCritIdList;
	}
	
	
	@Override
	public void remove(Long critId) throws ApplicationStorageException
	{
		super.remove(critId);
	}
}
