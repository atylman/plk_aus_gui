package com.nsn.gdo.feniks.uan.commons.core.manager;


import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanHuntingList;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanHuntingListManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;
import com.nsn.ploc.feniks.admingui.exception.ApplicationStorageException;


@Stateless(name="UanHuntingListManagerBean")
@RemoteBinding(jndiBinding=UanHuntingListManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanHuntingListManagerBean extends BaseManager<UanHuntingList, Long> implements UanHuntingListManager {

	Log logger = LogFactory.getLog(UanHuntingListManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	
	@Override
	protected Class<UanHuntingList> getEntityClass() {
		return UanHuntingList.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}


	@Override
	public UanHuntingList getHuntingList(Long hlId) {
		return getEntityManager().find(UanHuntingList.class, hlId);
		
	}

	@Override
	public void removeHuntingList(Long hlId) throws ApplicationStorageException {
		super.remove(hlId);
		
	}
	
	@Override
	public BigDecimal getNextSequence() {
		Query namedQuery1 = getEntityManager().createNativeQuery("Select UAN.HUNTING_ID_SQ.NEXTVAL from Dual");
		@SuppressWarnings("unused")
		BigDecimal nextSequence1 = ((BigDecimal)namedQuery1.getResultList().get(0));		
		Query namedQuery = getEntityManager().createNativeQuery("Select UAN.HUNTING_ID_SQ.CURRVAL from Dual");
		return ((BigDecimal)namedQuery.getResultList().get(0));
	}
		
}
