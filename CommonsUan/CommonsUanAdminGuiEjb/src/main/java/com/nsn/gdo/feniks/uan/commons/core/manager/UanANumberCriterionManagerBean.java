package com.nsn.gdo.feniks.uan.commons.core.manager;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanANumberCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingConfiguration;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanScreening;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanANumberCriterionManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanCriterionManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanRoutingConfigurationManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanScreeningManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanSubscriptionManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants.UanCriterionCriteriaTypes;


@Stateless(name="UanANumberCriterionManagerBean")
@RemoteBinding(jndiBinding=UanANumberCriterionManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanANumberCriterionManagerBean extends BaseManager<UanANumberCriterion, Long> implements UanANumberCriterionManager {

	Log logger = LogFactory.getLog(UanANumberCriterionManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	@EJB
	UanSubscriptionManager subscriptionManager;
	
	@EJB
	UanScreeningManager screeningManager;
	
	@EJB
	UanCriterionManager criterionManager;
	
	@EJB
	UanRoutingConfigurationManager uanRoutingConfigManager;
	
	@Override
	public UanANumberCriterion getANumberCriteriaForSubscrId(Long subscriptionId) {
		UanANumberCriterion uanACriterion = null;
		Long criteriaTypeId;

		UanSubscription uanSubscription = subscriptionManager.getSubscriptionById(subscriptionId);
		UanScreening uanScreening = uanSubscription.getScreeningProfileID();
		UanCriterion uanCriterion = uanScreening.getCritId();
		
		if(uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.A_NUMBER.getValue()){
			criteriaTypeId = uanCriterion.getCritTypeId();
			uanACriterion = getEntityManager().find(UanANumberCriterion.class, criteriaTypeId); 
		}
		
		return uanACriterion;
	}
	
	
	@Override
	public Long getANumberCriteriaIdForSubscrId(Long subscriptionId) {
		Long criteriaTypeId = null;

		UanSubscription uanSubscription = subscriptionManager.getSubscriptionById(subscriptionId);
		UanScreening uanScreening = uanSubscription.getScreeningProfileID();
		UanCriterion uanCriterion = uanScreening.getCritId();
	
		if(uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.A_NUMBER.getValue()){
			criteriaTypeId = uanCriterion.getCritTypeId();
		}
		
		return criteriaTypeId;
	}
	
	
	public BigDecimal getANumberCriterionNextSequence(){
		Query namedQuery1 = getEntityManager().createNativeQuery("Select UAN.CRITERION_TYPE_ID_SQ.NEXTVAL from Dual");
		@SuppressWarnings("unused")
		BigDecimal nextSequence1 = ((BigDecimal)namedQuery1.getResultList().get(0));		
		Query namedQuery = getEntityManager().createNativeQuery("Select UAN.CRITERION_TYPE_ID_SQ.CURRVAL from Dual");
		return ((BigDecimal)namedQuery.getResultList().get(0));
	}
	
	
	public List<Long> getAllANumCritIdForSubscription(Long subscriptionId) {
		List<Long> anumCritIdList = new ArrayList<Long>();
		UanRoutingConfiguration oldRoutingConfiguration = null;
		UanRoutingConfiguration newRoutingConfiguration = null;
		
		UanRoutingConfiguration uanRoutConfig = uanRoutingConfigManager.getRoutingConfigBySubsId(subscriptionId);

		if(uanRoutConfig.getCritID().getCritType().intValue() == UanCriterionCriteriaTypes.A_NUMBER.getValue()) {
			anumCritIdList.add(uanRoutConfig.getCritID().getCritTypeId());
		}
		
		newRoutingConfiguration = uanRoutConfig.getCrrIDForMatch();
		
		if(newRoutingConfiguration != null){
			while(newRoutingConfiguration.getCrrIDForNomatch() != null) {
				oldRoutingConfiguration = newRoutingConfiguration;
				newRoutingConfiguration = oldRoutingConfiguration.getCrrIDForNomatch();
				if(oldRoutingConfiguration.getCritID().getCritType().intValue() == UanCriterionCriteriaTypes.A_NUMBER.getValue()){
					anumCritIdList.add(oldRoutingConfiguration.getCritID().getCritTypeId());
				}				
			}
			if(newRoutingConfiguration.getCrrIDForNomatch() == null){
				anumCritIdList.add(newRoutingConfiguration.getCritID().getCritTypeId());
			}
		}
		return anumCritIdList;
	}
	
	@Override
	protected Class<UanANumberCriterion> getEntityClass() {
		return UanANumberCriterion.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	public UanANumberCriterion getANumberCriterionByCritId(Long critId)
	{
		Query query = getEntityManager().createQuery("select o from " + getEntityClass().getSimpleName() + " o where o.id = ?");
		query.setParameter(1, critId);
		return (UanANumberCriterion)query.getResultList().get(0);
	}
		
}
