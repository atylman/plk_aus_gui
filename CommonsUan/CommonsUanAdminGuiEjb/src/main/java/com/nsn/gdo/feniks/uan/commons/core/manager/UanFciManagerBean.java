package com.nsn.gdo.feniks.uan.commons.core.manager;


import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanFci;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanFciManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;


@Stateless(name="UanFciManagerBean")
@RemoteBinding(jndiBinding=UanFciManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanFciManagerBean extends BaseManager<UanFci, Long> implements UanFciManager {

	Log logger = LogFactory.getLog(UanFciManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	
	@Override
	public UanFci getFCIById(Long id) {
		return super.get(id); 
	}

	@Override
	public List<UanFci> getAll() {
		return super.getAll();
	}
	
	@Override
	protected Class<UanFci> getEntityClass() {
		return UanFci.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
		
}
