package com.nsn.gdo.feniks.uan.commons.core.manager;


import java.math.BigDecimal;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingConfiguration;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanScreening;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanTimeScheduleCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanCriterionManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanScreeningManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanSubscriptionManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanTimeScheduleCriterionManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants.UanCriterionCriteriaTypes;


@Stateless(name="UanTimeScheduleCriterionManagerBean")
@RemoteBinding(jndiBinding=UanTimeScheduleCriterionManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanTimeScheduleCriterionManagerBean extends BaseManager<UanTimeScheduleCriterion, Long> implements UanTimeScheduleCriterionManager {

	Log logger = LogFactory.getLog(UanTimeScheduleCriterionManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	@EJB
	UanSubscriptionManager subscriptionManager;
	
	@EJB
	UanScreeningManager screeningManager;
	
	@EJB
	UanCriterionManager criterionManager;
	
	@Override
	public UanTimeScheduleCriterion getTimeScheduleCriteriaForSubscrId(Long subscriptionId) {
		UanTimeScheduleCriterion uanTimeScheduleCriterion = null;
		Long criteriaTypeId;
		try{
			UanSubscription uanSubscription = subscriptionManager.getSubscriptionById(subscriptionId);
			UanScreening uanScreening = uanSubscription.getScreeningProfileID();
			if(uanScreening != null){
				UanCriterion uanCriterion = uanScreening.getCritId();
				
				if(uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.TIME_OF_DAY.getValue()){
					criteriaTypeId = uanCriterion.getCritTypeId();
					uanTimeScheduleCriterion = getEntityManager().find(UanTimeScheduleCriterion.class, criteriaTypeId); 
				}
			}
		}
		catch (Exception e) {
			logger.error("Error occured while extracting TimeSchedule from DB for subscriptionId =" + subscriptionId, e);
		}
		
		return uanTimeScheduleCriterion;
	}
	
	@Override
	public UanTimeScheduleCriterion getTimeScheduleCriteriaFromRoutingConfig(Long subscriptionId) {
		UanTimeScheduleCriterion uanTimeScheduleCriterion = null;
		Long criteriaTypeId;
		try{
			UanSubscription uanSubscription = subscriptionManager.getSubscriptionById(subscriptionId);
			UanRoutingConfiguration uanRoutingConfiguration = uanSubscription.getRrID();
			UanCriterion uanCriterion = uanRoutingConfiguration.getCritID();
			
			if(uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.TIME_OF_DAY.getValue()){
				criteriaTypeId = uanCriterion.getCritTypeId();
				uanTimeScheduleCriterion = getEntityManager().find(UanTimeScheduleCriterion.class, criteriaTypeId); 
			}
		}
		catch (Exception e) {
			logger.error("Error occured while extracting TimeSchedule from DB for subscriptionId =" + subscriptionId, e);

		}
		
		return uanTimeScheduleCriterion;
	}
	
	@Override
	public Long getTimeScheduleCriteriaIdForSubscrId(Long subscriptionId) {
		Long criteriaTypeId = null;
		UanSubscription uanSubscription = subscriptionManager.getSubscriptionById(subscriptionId);
		UanScreening uanScreening = uanSubscription.getScreeningProfileID();
		UanCriterion uanCriterion = uanScreening.getCritId();
	
		if(uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.TIME_OF_DAY.getValue()){
			criteriaTypeId = uanCriterion.getCritTypeId();
		}
		
		return criteriaTypeId;
	}
	
	@Override
	public Long getTimeScheduleCriteriaIdFromRoutingConfig(Long subscriptionId) {
		Long criteriaTypeId = null;

		UanSubscription uanSubscription = subscriptionManager.getSubscriptionById(subscriptionId);
		UanRoutingConfiguration uanRoutingConfiguration = uanSubscription.getRrID();
		UanCriterion uanCriterion = uanRoutingConfiguration.getCritID();
	
		if(uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.TIME_OF_DAY.getValue()){
			criteriaTypeId = uanCriterion.getCritTypeId();
		}
		
		return criteriaTypeId;
	}
	
	@SuppressWarnings("unused")
	public BigDecimal getTimeSchdCriterionNextSequence(){
		Query namedQuery1 = getEntityManager().createNativeQuery("Select UAN.CRITERION_TYPE_ID_SQ.NEXTVAL from Dual");

		BigDecimal nextSequence1 = ((BigDecimal)namedQuery1.getResultList().get(0));		
		Query namedQuery = getEntityManager().createNativeQuery("Select UAN.CRITERION_TYPE_ID_SQ.CURRVAL from Dual");
		return ((BigDecimal)namedQuery.getResultList().get(0));
	}
	
	@Override
	protected Class<UanTimeScheduleCriterion> getEntityClass() {
		return UanTimeScheduleCriterion.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
		
}
