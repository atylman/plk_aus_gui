package com.nsn.gdo.feniks.uan.commons.core.manager;


import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanSubscriptionManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;


@Stateless(name="UanSubscriptionManagerBean")
@RemoteBinding(jndiBinding=UanSubscriptionManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanSubscriptionManagerBean extends BaseManager<UanSubscription, Long> implements UanSubscriptionManager {

	Log logger = LogFactory.getLog(UanSubscriptionManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	
	@Override
	public UanSubscription getSubscriptionById(Long id) {
		return super.get(id); 
	}
	
	@Override
	protected Class<UanSubscription> getEntityClass() {
		return UanSubscription.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public List getSubscriptionAsListById(Long id){
		Query query = getEntityManager().createQuery("select o from " + getEntityClass().getSimpleName() + " o where o.subscriptionID = ?");
		query.setParameter(1, id);
		return query.getResultList();
	}
		
}
