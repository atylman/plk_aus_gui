package com.nsn.gdo.feniks.uan.commons.core.manager;


import java.math.BigDecimal;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingConfiguration;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanRoutingConfigurationManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanSubscriptionManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;


@Stateless(name="UanRoutingConfigurationManagerBean")
@RemoteBinding(jndiBinding=UanRoutingConfigurationManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanRoutingConfigurationManagerBean extends BaseManager<UanRoutingConfiguration, Long> implements UanRoutingConfigurationManager {

	Log logger = LogFactory.getLog(UanRoutingConfigurationManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	@EJB
	UanSubscriptionManager subscriptionManager;
	
	@Override
	public UanRoutingConfiguration getRoutingConfigById(Long rrId) {
		return super.get(rrId); 
	}
	
	@Override
	protected Class<UanRoutingConfiguration> getEntityClass() {
		return UanRoutingConfiguration.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Override
	public UanRoutingConfiguration getRoutingConfigBySubsId(Long subsId)
	{
		UanSubscription uanSubscription = subscriptionManager.getSubscriptionById(subsId);
		return uanSubscription.getRrID();
	}
	
	public BigDecimal getRoutingConfigNextSequence(){
		Query namedQuery1 = getEntityManager().createNativeQuery("Select UAN.RR_ID_SQ.NEXTVAL from Dual");
		@SuppressWarnings("unused")
		BigDecimal nextSequence1 = ((BigDecimal)namedQuery1.getResultList().get(0));		
		Query namedQuery = getEntityManager().createNativeQuery("Select UAN.RR_ID_SQ.CURRVAL from Dual");
		return ((BigDecimal)namedQuery.getResultList().get(0));
	}
		
}
