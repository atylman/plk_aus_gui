package com.nsn.gdo.feniks.uan.commons.core.manager;


import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanIvrCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingConfiguration;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanIvrCriterionManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanSubscriptionManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants.UanCriterionCriteriaTypes;


@Stateless(name="UanIvrCriterionManagerBean")
@RemoteBinding(jndiBinding=UanIvrCriterionManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanIvrCriterionManagerBean extends BaseManager<UanIvrCriterion, Long> implements UanIvrCriterionManager {

	Log logger = LogFactory.getLog(UanIvrCriterionManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	@EJB
	UanSubscriptionManager subscriptionManager;
	
	
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List getIvrCriteriaForSubscrId(Long subscriptionId) {
		List ivrCriterionList = null;
		Long criteriaTypeId;
		
		// get  data from uan_subscription table(screening profile id)
		UanSubscription uanSubscription = subscriptionManager.getSubscriptionById(subscriptionId);
		UanRoutingConfiguration uanRoutingConfiguration = uanSubscription.getRrID();
		
		UanCriterion uanCriterion = uanRoutingConfiguration.getCritID();
		if(uanCriterion != null){
			if(uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.IVR.getValue()){
				criteriaTypeId = uanCriterion.getCritTypeId();
				ivrCriterionList = getIvrCriterionList(criteriaTypeId);
			}
		}
		return ivrCriterionList;
	}
	
	@Override
	public Long getIvrCriteriaIdForSubscrId(Long subscriptionId) {
		Long criteriaTypeId = -1L;
		
		// get  data from uan_subscription table(screening profile id)
		UanSubscription uanSubscription = subscriptionManager.getSubscriptionById(subscriptionId);
		UanRoutingConfiguration uanRoutingConfiguration = uanSubscription.getRrID();
	
		UanCriterion uanCriterion = uanRoutingConfiguration.getCritID();
		if(uanCriterion != null){
			if(uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.IVR.getValue()){
				criteriaTypeId = uanCriterion.getCritTypeId();
			}
		}
		return criteriaTypeId;
	}
	
	
	public BigDecimal getIvrCriterionNextSequence(){
		Query namedQuery1 = getEntityManager().createNativeQuery("Select UAN.CRITERION_TYPE_ID_SQ.NEXTVAL from Dual");
		@SuppressWarnings("unused")
		BigDecimal nextSequence1 = ((BigDecimal)namedQuery1.getResultList().get(0));		
		Query namedQuery = getEntityManager().createNativeQuery("Select UAN.CRITERION_TYPE_ID_SQ.CURRVAL from Dual");
		return ((BigDecimal)namedQuery.getResultList().get(0));
	}
	
	@Override
	protected Class<UanIvrCriterion> getEntityClass() {
		return UanIvrCriterion.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@SuppressWarnings("unchecked")
	public  List<UanIvrCriterion> getIvrCriterionList(Long critId)
	{
		Query query = getEntityManager().createQuery("select o from " + getEntityClass().getSimpleName() + " o where o.id.ivrCritId = ?");
		query.setParameter(1, critId);
		return query.getResultList();
	}

	}
