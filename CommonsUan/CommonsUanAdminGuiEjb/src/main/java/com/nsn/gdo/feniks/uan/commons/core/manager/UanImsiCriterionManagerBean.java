package com.nsn.gdo.feniks.uan.commons.core.manager;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanImsiCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingConfiguration;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanSubscription;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanImsiCriterionManager;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanSubscriptionManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants.UanCriterionCriteriaTypes;

@Stateless(name="UanImsiCriterionManagerBean")
@RemoteBinding(jndiBinding=UanImsiCriterionManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanImsiCriterionManagerBean extends BaseManager<UanImsiCriterion, Long> implements UanImsiCriterionManager{

	Log logger = LogFactory.getLog(UanImsiCriterionManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	@EJB
	UanSubscriptionManager subscriptionManager;
	
	
	@Override
	@SuppressWarnings("rawtypes")
	public List getImsiCriteriaForSubscrId(Long subscriptionId) {
		List imsiCriterionList = null;
		Long criteriaTypeId;
		
		// get  data from uan_subscription table(screening profile id)
		UanSubscription uanSubscription = subscriptionManager.getSubscriptionById(subscriptionId);
		UanRoutingConfiguration uanRoutingConfiguration = uanSubscription.getRrID();
		
		UanCriterion uanCriterion = uanRoutingConfiguration.getCritID();
		if(uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.IMSI.getValue()){
			criteriaTypeId = uanCriterion.getCritTypeId();
			imsiCriterionList = getImsiCriterionList(criteriaTypeId);
		}
		return imsiCriterionList;
	}
	
	@Override
	public Long getImsiCriteriaIdForSubscrId(Long subscriptionId) {
		Long criteriaTypeId = null;
		
		// get  data from uan_subscription table(screening profile id)
		UanSubscription uanSubscription = subscriptionManager.getSubscriptionById(subscriptionId);
		UanRoutingConfiguration uanRoutingConfiguration = uanSubscription.getRrID();
	
		UanCriterion uanCriterion = uanRoutingConfiguration.getCritID();

		if(uanCriterion.getCritType().intValue() == UanCriterionCriteriaTypes.IMSI.getValue()){
			criteriaTypeId = uanCriterion.getCritTypeId();
		}
		return criteriaTypeId;
	}
	
	public BigDecimal getImsiCriterionNextSequence(){
		Query namedQuery1 = getEntityManager().createNativeQuery("Select UAN.CRITERION_TYPE_ID_SQ.NEXTVAL from Dual");
		@SuppressWarnings("unused")
		BigDecimal nextSequence1 = ((BigDecimal)namedQuery1.getResultList().get(0));		
		Query namedQuery = getEntityManager().createNativeQuery("Select UAN.CRITERION_TYPE_ID_SQ.CURRVAL from Dual");
		return ((BigDecimal)namedQuery.getResultList().get(0));
	}	
	
	@Override
	protected Class<UanImsiCriterion> getEntityClass() {
		return UanImsiCriterion.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}	
	
	@SuppressWarnings("rawtypes")
	private List getImsiCriterionList(Long critId)
	{
		Query query = getEntityManager().createQuery("select o from " + getEntityClass().getSimpleName() + " o where o.id.imsiCritId = ?");
		query.setParameter(1, critId);
		return query.getResultList();
	}	
}
