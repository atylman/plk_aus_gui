package com.nsn.gdo.feniks.uan.commons.core.manager;

import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanConfigParam;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanConfigParamManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;

/**
 * Session Bean for UAN Configuration parameters management
 * @author ggne0151
 *
 */
@Stateless(name="UanConfigParamManagerBean")
@RemoteBinding(jndiBinding=UanConfigParamManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanConfigParamManagerBean extends BaseManager<UanConfigParam, String> implements UanConfigParamManager {

	@PersistenceContext(unitName=UanConstants.PERSISTENCE_UNIT_NAME)
	protected EntityManager em;		
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UanConfigParam> findByServiceId(Long serviceId) {
		Query query = getEntityManager().createQuery("select o from " + getEntityClass().getSimpleName() + " o where o.serviceType = ?");
		query.setParameter(1, serviceId);
		return query.getResultList();
	}

	@Override
	protected Class<UanConfigParam> getEntityClass() {
		return UanConfigParam.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

}
