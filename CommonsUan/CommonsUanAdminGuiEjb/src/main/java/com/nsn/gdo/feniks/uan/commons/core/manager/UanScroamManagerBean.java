package com.nsn.gdo.feniks.uan.commons.core.manager;


import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanScroamInfo;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanScroamManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;


@Stateless(name="UanScroamManagerBean")
@RemoteBinding(jndiBinding=UanScroamManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanScroamManagerBean extends BaseManager<UanScroamInfo, Long> implements UanScroamManager {

	Log logger = LogFactory.getLog(UanScroamManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	
	@Override
	public UanScroamInfo getScroamById(Long id) {
		return super.get(id); 
	}
	
	@Override
	protected Class<UanScroamInfo> getEntityClass() {
		return UanScroamInfo.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
		
}
