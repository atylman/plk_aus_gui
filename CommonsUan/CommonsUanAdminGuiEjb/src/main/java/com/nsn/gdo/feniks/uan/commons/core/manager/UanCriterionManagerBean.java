package com.nsn.gdo.feniks.uan.commons.core.manager;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.RemoteBinding;

import com.nsn.ploc.feniks.admingui.core.BaseManager;
import com.nsn.ploc.feniks.admingui.core.interceptor.ExceptionMapperInterceptor;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanCriterion;
import com.nsn.gdo.feniks.admingui.core.uan.model.UanRoutingConfiguration;
import com.nsn.gdo.feniks.admingui.core.uan.service.UanCriterionManager;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants;
import com.nsn.gdo.feniks.admingui.core.uan.util.UanConstants.UanCriterionCriteriaTypes;


@Stateless(name="UanCriterionManagerBean")
@RemoteBinding(jndiBinding=UanCriterionManager.REMOTE_JNDI_NAME)
@Interceptors(ExceptionMapperInterceptor.class)
public class UanCriterionManagerBean extends BaseManager<UanCriterion, Long> implements UanCriterionManager {

	Log logger = LogFactory.getLog(UanCriterionManagerBean.class);
	
	@PersistenceContext(unitName = UanConstants.PERSISTENCE_UNIT_NAME) 
	protected EntityManager entityManager;
	
	
	@Override
	public UanCriterion getCriterionById(Long critId) {
		return super.get(critId); 
	}
	
	@Override
	protected Class<UanCriterion> getEntityClass() {
		return UanCriterion.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	public BigDecimal getCriterionNextSequence(){
		Query namedQuery1 = getEntityManager().createNativeQuery("Select UAN.CRITERION_ID_SQ.NEXTVAL from Dual");
		
		@SuppressWarnings("unused")
		BigDecimal nextSequence1 = ((BigDecimal)namedQuery1.getResultList().get(0));		
		Query namedQuery = getEntityManager().createNativeQuery("Select UAN.CRITERION_ID_SQ.CURRVAL from Dual");
		return ((BigDecimal)namedQuery.getResultList().get(0));
	}
		
	public List<Long> getAllAnumTypeCriterionIdForChainedRouteConfig(UanRoutingConfiguration firstChainedRouteConfigObj) {
		List<Long> anumCritrionIdList = new ArrayList<Long>();
		UanRoutingConfiguration oldRoutingConfiguration = null;
		UanRoutingConfiguration newRoutingConfiguration = null;
		
		newRoutingConfiguration = firstChainedRouteConfigObj;
		
		if(newRoutingConfiguration != null){
			while(newRoutingConfiguration.getCrrIDForNomatch() != null) {
				oldRoutingConfiguration = newRoutingConfiguration;
				newRoutingConfiguration = oldRoutingConfiguration.getCrrIDForNomatch();
				if(oldRoutingConfiguration.getCritID().getCritType().intValue() == UanCriterionCriteriaTypes.A_NUMBER.getValue()){
					anumCritrionIdList.add(oldRoutingConfiguration.getCritID().getCritId());
				}				
			}
			if(newRoutingConfiguration.getCrrIDForNomatch() == null){
				anumCritrionIdList.add(newRoutingConfiguration.getCritID().getCritId());
			}
		}
		return anumCritrionIdList;
	}
	
	
	public List<Long> getAllZoneTypeCriterionIdForChainedRouteConfig(UanRoutingConfiguration firstChainedRouteConfigObj) {
		List<Long> zoneCritrionIdList = new ArrayList<Long>();
		UanRoutingConfiguration oldRoutingConfiguration = null;
		UanRoutingConfiguration newRoutingConfiguration = null;
		
		newRoutingConfiguration = firstChainedRouteConfigObj;
		
		if(newRoutingConfiguration != null){
			while(newRoutingConfiguration.getCrrIDForNomatch() != null) {
				oldRoutingConfiguration = newRoutingConfiguration;
				newRoutingConfiguration = oldRoutingConfiguration.getCrrIDForNomatch();
				if(oldRoutingConfiguration.getCritID().getCritType().intValue() == UanCriterionCriteriaTypes.ZONE.getValue()){
					zoneCritrionIdList.add(oldRoutingConfiguration.getCritID().getCritId());
				}				
			}
			if(newRoutingConfiguration.getCrrIDForNomatch() == null){
				zoneCritrionIdList.add(newRoutingConfiguration.getCritID().getCritId());
			}
		}
		return zoneCritrionIdList;
	}
	
}
